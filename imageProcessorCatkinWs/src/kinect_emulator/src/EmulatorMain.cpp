//
// Created by raigo on 8.04.17.
//

#include "ros/ros.h"
#include "std_msgs/String.h"
#include <image_transport/image_transport.h>
#include <string>
#include <cv_bridge/cv_bridge.h>
#include <opencv2/opencv.hpp>

int main(int argc, char **argv) {
    ros::init(argc, argv, "kinectEmulator");

    ros::NodeHandle nh;
    //TODO a portable path
    std::string basePath = "/home/raigo/Repod/Loputoo/Robot_recording_system/imageProcessorCatkinWs/TestCases";
    //TODO configurable with start arguments or sth else
    std::string taskName = "LineFollowing";
    std::string testNumber = "2";
    std::cout << "Starting kinect emulator: " << taskName << " (" << testNumber << ")" << std::endl;
    ros::Rate loop_rate(0.1);
    ros::Publisher pub = nh.advertise<sensor_msgs::Image>("/kinect2/hd/image_color", 1);
    for (int i = 1; i < 10; i++) {
        if (nh.ok()) {
            cv_bridge::CvImage cv_image;
            std::string path = basePath + "/" + taskName + "/" + testNumber + "/" + std::to_string(1) + ".png";
            std::cout << "Publishing image from: " << path << std::endl;
            cv_image.image = cv::imread(path);



            if (!cv_image.image.data) {
                std::cout << "Could not open or find the image" << std::endl;
                continue;
            }

            cv_image.encoding = "bgr8";
            sensor_msgs::Image ros_image;
            cv_image.toImageMsg(ros_image);
            std::cout << "Publishing image no " << i << std::endl;
            cv::waitKey(0);
            pub.publish(ros_image);
            loop_rate.sleep();
        }
    }

    return 0;
}