//
// Created by raigo on 26.03.17.
//

#include "DatabaseService.h"
#include <future>
#include "ros/ros.h"
#include "../RuntimeVariables.h"
#include "../net/HttpService.h"
#include "../utilclasses/Base64.h"
#include "../utilclasses/JsonSerialiser.h"
#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>


static std::mutex mutex;

void save(cv::Mat image2, long imageID, long runID) {

    ROS_DEBUG("Method start");
    //cv::Mat image2 = image.clone();

    ROS_DEBUG("Image save: Saving image id %d for run %d", imageID, runID);

    if (!image2.data) {
        ROS_ERROR("Image save: No image to send");
    }

    cv::Mat continuousRGBA;
    /*uchar* camData = new uchar[image2.total()*4];
    cv::Mat continuousRGBA(image2.size(), CV_8UC4, camData);

    ROS_DEBUG("New image created");
    ROS_DEBUG_STREAM("Image size " << image2.size().height << " " << image2.size().width << " " << image2.total());

    cv::cvtColor(image2, continuousRGBA, CV_BGR2RGBA);

    ROS_DEBUG("Image conversion done");*/

    std::vector<cv::Mat> matChannels;
    cv::split(image2, matChannels);

    // create alpha channel
    cv::Mat alpha = matChannels.at(0) + matChannels.at(1) + matChannels.at(2);
    matChannels.push_back(alpha);

    cv::merge(matChannels, continuousRGBA);

    ROS_DEBUG("Image conversion done");

    size_t sizeOfArray = continuousRGBA.total() * continuousRGBA.elemSize();
    std::string base64img = Base64::base64_encode(continuousRGBA.data, sizeOfArray);
    HttpService* httpService = new HttpService();
    std::string response =
            httpService
                    ->sendPostRequest(
                            RuntimeVariables::imageServer,
                            RuntimeVariables::imageServerPort,
                            "/img/" + std::to_string(runID) + "/" + std::to_string(imageID),
                            JsonSerialiser::composeImageRequestBody(continuousRGBA.cols, continuousRGBA.rows, base64img));
    ROS_DEBUG("Image save: Image (%d, run %d) saving result: %s", imageID, runID, response.c_str());


    mutex.unlock();
}

void DatabaseService::saveRun(std::shared_ptr<Run> run, Utils::TaskAssessment assessment) {

    pqxx::connection* conn = buildConnection();

    if (conn == nullptr) {
        return;
    }

    conn->prepare("saveRun",
                  "INSERT INTO run(run_id, team_id, task_id, framerate, ms_taken, score, run_data, distance, last_frame_id)"
                          " VALUES ($1, (SELECT team_id FROM team WHERE tag_id = $2), $3,"
                          " $4, $5, $6, $7, $8, $9)");

    try {
        pqxx::work work(*conn);
        work.prepared("saveRun")
                (run->getRunID())
                (run->getTeamID())
                (run->getTaskID())
                (run->getFrameRate())
                (assessment.taskTime)
                (assessment.score)
                (assessment.jsonData)
                (assessment.distance)
                (run->getCurrentImageID()).exec();
        work.commit();
    } catch (const std::exception &e) {
        ROS_ERROR(e.what());
    }
    dropConnection(conn);
}

long DatabaseService::requestRunId() {
    ROS_DEBUG("Getting new Run ID from database");
    long id = getLong("SELECT NEXTVAL('run_id_seq');");
    if (id == -1) {
        ROS_ERROR("Unable to get RUN ID, setting to -1");
    }
    ROS_DEBUG("Got run ID: %d", id);
    return id;
    //return 5;
}

long DatabaseService::getLong(std::string sql) {

    ROS_DEBUG("Requesting long: %s", sql.c_str());

    pqxx::connection* conn = buildConnection();
    if (conn == nullptr) {
        return -1;
    }

    if (!conn->is_open()) {
        ROS_ERROR("Unable to get long from database Connection closed, unable to open. Setting to default: -1");
        return -1;
    }
    try {
        pqxx::work work(*conn);
        pqxx::result r = work.exec(sql);
        work.commit();
        long resultLong = r[0][0].as<long>();
        dropConnection(conn);
        return resultLong;
    } catch (const std::exception &e) {
        ROS_ERROR(e.what());
        ROS_ERROR("Returning -1");
        dropConnection(conn);
        return -1;
    }

}

void DatabaseService::saveImage(cv::Mat &image, long imageID, long runID) {
    ROS_DEBUG("Calling image async method");
    mutex.lock();
    cv::Mat smallImage;
    cv::resize(image, smallImage, cv::Size(640, 360), cv::INTER_LINEAR);
    //std::async(std::launch::async, &DatabaseService::saveImageCallback, this, image, imageID, runID);
    //saveImageCallback(image, imageID, runID);
    std::thread t1(save, smallImage, imageID, runID);
    t1.detach();
}

void DatabaseService::saveImageCallback(cv::Mat image, long imageID, long runID) {
    ROS_DEBUG("Image save: Saving image id %d for run %d", imageID, runID);

    if (!image.data) {
       ROS_ERROR("Image save: No image to send");
    }

    uchar* camData = new uchar[image.total()*4];
    cv::Mat continuousRGBA(image.size(), CV_8UC4, camData);
    cv::cvtColor(image, continuousRGBA, CV_BGR2RGBA, 4);

    size_t sizeOfArray = continuousRGBA.total() * continuousRGBA.elemSize();
    std::string base64img = Base64::base64_encode(continuousRGBA.data, sizeOfArray);
    HttpService* httpService = new HttpService();
    std::string response =
            httpService
                    ->sendPostRequest(
                            RuntimeVariables::imageServer,
                            RuntimeVariables::imageServerPort,
                            "/img/" + std::to_string(runID) + "/" + std::to_string(imageID),
                            JsonSerialiser::composeImageRequestBody(image.cols, image.rows, base64img));
    ROS_DEBUG("Image save: Image (%d, run %d) saving result: %s", imageID, runID, response.c_str());
}

pqxx::connection * DatabaseService::buildConnection() {
    ROS_DEBUG("Trying to connect to database %s", RuntimeVariables::dbConnectionString.c_str());
    if (RuntimeVariables::dbConnectionString.empty()) {
        ROS_ERROR("No DB connection string provided! Cannot build DB connection");
        return nullptr;
    }

    pqxx::connection* conn;
    try {
        conn = new pqxx::connection(RuntimeVariables::dbConnectionString);
        if (conn->is_open()) {
            ROS_INFO("Opened database successfully: %s", conn->dbname());
        } else {
            ROS_ERROR("Can't open database for unknown reasons.");
            return nullptr;
        }

        //TEST CONNECTION:

        //pqxx::work w(*conn);
        //pqxx::result r = w.exec("SELECT 1");
        //w.commit();
        //ROS_INFO("Testing postgresql connection: %d", r[0][0].as<int>());

    } catch (const std::exception &e) {
        ROS_ERROR(e.what());
        return nullptr;
    }


    return conn;
}

void DatabaseService::dropConnection(pqxx::connection *conn) {
    if (conn != nullptr && conn->is_open()) {
        conn->disconnect();
    }
}