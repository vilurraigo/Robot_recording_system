//
// Created by raigo on 26.03.17.
//

#ifndef IMAGEPROCESSOR_DATABASESERVICE_H
#define IMAGEPROCESSOR_DATABASESERVICE_H


#include <memory>
#include <pqxx/pqxx>
#include <opencv2/opencv.hpp>
#include <mutex>
#include "../model/Run.h"

class DatabaseService {

public:

    void saveRun(std::shared_ptr<Run> run, Utils::TaskAssessment assessment);

    void saveImage(cv::Mat &image, long imageID, long runID);

    long requestRunId();


private:
    pqxx::connection* buildConnection();
    void dropConnection(pqxx::connection* conn);
    long getLong(std::string sql);


    void saveImageCallback(cv::Mat image, long imageID, long runID);
};




#endif //IMAGEPROCESSOR_DATABASESERVICE_H
