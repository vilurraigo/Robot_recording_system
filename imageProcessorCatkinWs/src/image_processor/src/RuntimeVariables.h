//
// Created by raigo on 16.03.17.
//

#ifndef IMAGEPROCESSOR_RUNTIMEVARIABLES_H
#define IMAGEPROCESSOR_RUNTIMEVARIABLES_H

#include <string>
#include <image_processor/image_processor_configConfig.h>


class RuntimeVariables {
public:
    static double coordinateReferenceSize;
    static std::string imageTopic;
    static int maxEmptyFrames;
    static std::string dbConnectionString;

    static int frameRate;

    static double distanceFromTag;

    static double tag_size;
    static double fx;
    static double fy;
    static double px;
    static double py;

    static std::string imageServer;
    static int imageServerPort;

    static void reconfigure(image_processor::image_processor_configConfig &config);
};


#endif //IMAGEPROCESSOR_RUNTIMEVARIABLES_H
