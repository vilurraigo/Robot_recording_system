//
// Created by raigo on 15.03.17.
//

#include <memory>
#include "LineFollowerTask.h"
#include "../model/Run.h"
#include "ros/ros.h"


Utils::TaskAssessment LineFollowerTask::process(std::shared_ptr<Run> run, double referenceSize) {

    ROS_INFO("Line Follower task: Starting run end processing");

    Utils::TaskAssessment assessment;
    std::vector<std::shared_ptr<Frame>> frames = run->getFrames();
    std::shared_ptr<std::chrono::system_clock::time_point> startTime = frames.front()->getTimeStamp();
    std::shared_ptr<std::chrono::system_clock::time_point> endTime = frames.back()->getTimeStamp();

    assessment.taskTime = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::time_point_cast<std::chrono::seconds>(*endTime.get()) - std::chrono::time_point_cast<std::chrono::seconds>(*startTime.get())).count();
    assessment.distance = distance;
    assessment.score = (int) (10000 - distance);
    assessment.jsonData = "{}";

    ROS_INFO("Line follower task: Run ended with time: %d. Distance covered: %f. Score: %d", assessment.taskTime, assessment.distance, assessment.score);

    return assessment;
}

bool LineFollowerTask::isRunEndCondition(std::shared_ptr<Frame> frame, cv::Mat image, double referenceSize) {
    std::shared_ptr<Actor> robot;

    for (std::shared_ptr<Actor> actor : frame -> getActorsInFrame()) {
        if (actor->getType() == ActorType::ROBOT) {
            robot = actor;
            //std::cout << "Line follower task: found robot " << robot->toString() << " . " << actor->toString() << std::endl;
        }
        if (actor->getType() == ActorType::WAYPOINT && actor->getID() == finishWaypointID) {
            ROS_DEBUG("Line follower task: updating finish waypoint");
            this->finish = actor;
        }
    }


    if (robot == nullptr) {
        ROS_ERROR("Line follower task: Robot is not present, these frames should be filtered before getting to the task implementation");
        return true;
    }

    if (robotLocationInLastFrame != nullptr) {
        distance += Utils::calculateDistanceBetween(robotLocationInLastFrame->getX(), robotLocationInLastFrame->getY(), robot->getX(), robot->getY(), referenceSize);
    }


    if (finish == nullptr) {
        ROS_ERROR("Line follower task: finish has not been registered, cannot decide if run has ended.");
        return false;
    }
    double actualDistanceToWaypoint = Utils::calculateDistanceBetween(robot->getX(), robot->getY(), finish->getX(), finish->getY(), referenceSize);

    ROS_DEBUG("Line follower task: Robot location: x = %f, y = %f. End location: x = %f, y = %f", robot->getX(), robot->getY(), finish->getX(), finish->getY());
    ROS_DEBUG("Line follower task: Distance from end tag: %f", actualDistanceToWaypoint);
    if (minDistanceToWaypoint > actualDistanceToWaypoint) {
        ROS_INFO("Line follower task: End waypoint reached, ending run.");
        return true;
    }

    robotLocationInLastFrame = robot->getLocation();

    return false;
}

LineFollowerTask::LineFollowerTask() {
    this->distance = 0;
}


