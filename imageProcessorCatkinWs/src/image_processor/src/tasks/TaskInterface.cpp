//
// Created by raigo on 15.03.17.
//

#include "TaskInterface.h"

int TaskInterface::getRobotIDFromFrame(std::shared_ptr<Frame> frame) {

    for (std::shared_ptr<Actor> actor : frame->getActorsInFrame()) {
        if (actor->getType() == ActorType::ROBOT) {
            return actor->getID();
        }
    }

    return -1;
}

int TaskInterface::getTeamIDFromFrame(std::shared_ptr<Frame> frame) {
    for (std::shared_ptr<Actor> actor : frame->getActorsInFrame()) {
        if (actor->getType() == ActorType::TEAM) {
            return actor->getID();
        }
    }
    return -1;
}
