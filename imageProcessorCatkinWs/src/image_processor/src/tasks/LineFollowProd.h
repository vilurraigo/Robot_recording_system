//
// Created by raigo on 20.09.17.
//

#ifndef IMAGE_PROCESSOR_LINEFOLLOWPROD_H
#define IMAGE_PROCESSOR_LINEFOLLOWPROD_H


#include <opencv2/opencv.hpp>
#include "TaskInterface.h"

/**
 * This task is defined as: "Follow tags in increasing order until you reach tag no 2 (End tag).
 *  If some tag is missed, then it is an instant failure. Has to have at least one tag (in extra to start tag)
 */
class LineFollowProd : public TaskInterface {

public:

    LineFollowProd();

    Utils::TaskAssessment process(std::shared_ptr<Run> run, double referenceSize) override;

    bool isRunEndCondition(std::shared_ptr<Frame> frame, cv::Mat image, double referenceSize) override;

private:

    bool fail = true;
    ActorPtr nextTagActor;
    int nextId;
    std::shared_ptr<Location> robotLocationInLastFrame = nullptr;
    double distance;

};


#endif //IMAGE_PROCESSOR_LINEFOLLOWPROD_H
