//
// Created by raigo on 20.09.17.
//

#include "LineFollowProd.h"
#include "../MainProcessor.h"
#include "../utilclasses/ActorSearcher.h"
#include <ros/ros.h>


LineFollowProd::LineFollowProd() {
    nextId = 0;
    distance = 0;
}

Utils::TaskAssessment LineFollowProd::process(std::shared_ptr<Run> run, double referenceSize) {

    ROS_INFO("Line Follower task: Starting run end processing");

    Utils::TaskAssessment assessment;
    std::vector<std::shared_ptr<Frame>> frames = run->getFrames();
    std::shared_ptr<std::chrono::system_clock::time_point> startTime = frames.front()->getTimeStamp();
    std::shared_ptr<std::chrono::system_clock::time_point> endTime = frames.back()->getTimeStamp();

    assessment.taskTime = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::time_point_cast<std::chrono::seconds>(*endTime.get()) - std::chrono::time_point_cast<std::chrono::seconds>(*startTime.get())).count();
    assessment.distance = distance;
    fail? assessment.score = 0 : assessment.score = 100;
    assessment.jsonData = "{}";

    ROS_INFO("Line follower task: Run ended with time: %d. Distance covered: %f. Score: %d", assessment.taskTime, assessment.distance, assessment.score);

    return assessment;
}

bool LineFollowProd::isRunEndCondition(std::shared_ptr<Frame> frame, cv::Mat image, double referenceSize) {

    ROS_DEBUG("Line follower task: Processing run step");

    if (frame == nullptr) {
        ROS_ERROR("Line follower task: Frame is nullptr!");
        return true;
    }

    ActorPtr robot = ActorSearcher::getFirstActorFromFrame(frame, ActorType::ROBOT);

    if (robotLocationInLastFrame != nullptr) {
        distance += Utils::calculateDistanceBetween(robotLocationInLastFrame->getX(), robotLocationInLastFrame->getY(), robot->getX(), robot->getY(), referenceSize);
    }

    robotLocationInLastFrame = robot->getLocation();

    ActorVector actors = frame->getActorsInFrame();
    double closestDistance = 50000;
    ActorPtr closestActor = nullptr;
    int possibleNextId = 10000;
    ActorPtr possibleNextActor = nullptr;
    bool nextIdSeen = false;
    bool canAdvance = false;

    for (ActorPtr &actor : actors) {
        if (actor->getType() == ActorType::WAYPOINT) {

            //Next ID processing:
            if (nextId < actor->getID() && actor->getID() < possibleNextId) {
                //Finds the smallest id that comes after the current tag

                possibleNextId = actor->getID();
                possibleNextActor = actor;
                ROS_DEBUG("LineFollow: Possible next tag: %d", possibleNextId);
            }

            if (nextId != 0) {
                //This section is only when next tag is known

                //Finding the closest actor:
                double currentTagDistance = Utils::calculateDistanceBetween(robot->getX(), robot->getY(), actor->getX(), actor->getY(), referenceSize);
                if (currentTagDistance < closestDistance || closestActor == nullptr) {
                    closestDistance = currentTagDistance;
                    closestActor = actor;
                }

                //Processing close to next id
                if (actor->getID() == nextId) {
                    ROS_DEBUG("LineFollow: Distance from next tag (id=%d): %f. Required: %f", nextId, currentTagDistance, RuntimeVariables::distanceFromTag);
                    nextIdSeen = true;
                    nextTagActor = actor;

                    if (currentTagDistance <= RuntimeVariables::distanceFromTag) {
                        ROS_DEBUG("LineFollow: Can advance to next tag (tag passed)");
                        canAdvance = true;
                    }
                }
            }
        }
    }


    if (!nextIdSeen && nextTagActor != nullptr) {
        //This means that the next tag was not in the frame, we will use the one saved from last
        double currentTagDistance = Utils::calculateDistanceBetween(robot->getX(), robot->getY(), nextTagActor->getX(), nextTagActor->getY(), referenceSize);
        ROS_DEBUG("LineFollow: Distance from next tag: %f. Required: %f", currentTagDistance, RuntimeVariables::distanceFromTag);
        if (currentTagDistance <= RuntimeVariables::distanceFromTag) {
            ROS_DEBUG("LineFollow: Can advance to next tag (last known position passed)");
            canAdvance = true;
        }
    }

    if (nextId == 0) {
        if (possibleNextActor == nullptr) {
            ROS_DEBUG("LineFollow: No next tag known but this is a first frame!");
            fail = false;
            return true;
        }
        nextId = possibleNextId;
        nextTagActor = possibleNextActor;
        ROS_DEBUG("LineFollow: First tag ID: %d", nextId);
    }

    if (canAdvance) {
        if (possibleNextActor == nullptr) {
            ROS_DEBUG("LineFollow: No next tag known, declaring a success!");
            fail = false;
            return true;
        }

        nextId = possibleNextId;
        nextTagActor = possibleNextActor;
        ROS_DEBUG("LineFollow: Next tag ID: %d", nextId);
    }

    return false;

    /*


    ActorVector wayPoints = ActorSearcher::getAllActorsFromFrame(frame, ActorType::WAYPOINT);
    addOrUpdateKnownWayPoints(wayPoints);

    ActorVector known = getknownWaypointsAsVector();

    ActorPtr closestActor = ActorSearcher::getActorClosestToTarget(robot, known);

    if (Utils::calculateDistanceBetween(robot->getX(), robot->getY(), closestActor->getX(), closestActor->getY(), referenceSize) <= RuntimeVariables::distanceFromTag) {

        if (closestActor->getID() == nextId) {
            int nextPossibleId = getNextId();
            if (nextPossibleId == 10000) {
                //There are no bigget ID-s, report success!
                return true;
            } else {
                nextId = nextPossibleId;
            }
        } else if (closestActor->getID() > nextId) {
            //Some Id was missed!!!
            fail = true;
            return false;
        }
        //Smaller ID-s we do not care!
    }
    //If not close enough to anyone, we also don't care
    return false;
     */
}
/*
void LineFollowProd::addOrUpdateKnownWayPoints(ActorVector &wayPoints) {

    auto it = wayPoints.begin();
    int lowestIdInFrame = 10000;

    for (it; it != wayPoints.end(); ++it) {
        if (it->get()->getID() != MainProcessor::START_WAYPOINT_ID) {
            knownWayPoints[it->get()->getID()] = *it;

            if (it->get()->getID() < lowestIdInFrame) {
                lowestIdInFrame = it->get()->getID();
            }
        }
    }

    if (nextId == -1) {
        nextId = lowestIdInFrame;
    }

}

int LineFollowProd::getNextId() {

    int currentNext = 10000;

    for (auto it = knownWayPoints.begin(); it != knownWayPoints.end(); it++) {
        if (it->first > nextId && it->first < currentNext) {
            currentNext = it->first;
        }
    }

    return currentNext;
}

ActorVector LineFollowProd::getknownWaypointsAsVector() {
    ActorVector vector;

    for(auto const& actorIt: knownWayPoints)
        vector.push_back(actorIt.second);

    return vector;
}
*/
