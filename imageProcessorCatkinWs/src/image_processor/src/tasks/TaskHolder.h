//
// Created by raigo on 16.03.17.
//

#ifndef IMAGEPROCESSOR_TASK_H
#define IMAGEPROCESSOR_TASK_H

#include <memory>
#include "TaskInterface.h"
#include "LineFollowerTask.h"
#include "../exceptions/NoTaskFoundException.h"
#include "LineFollowProd.h"

class TaskHolder {

public:
    static std::shared_ptr<TaskInterface> getTaskById(long id) {
        switch (id) {
            case 1:
                return std::make_shared<LineFollowerTask>(LineFollowerTask());
            case 0:
                return std::make_shared<LineFollowProd>(LineFollowProd());
            case 2:
                return std::make_shared<LineFollowProd>(LineFollowProd());
            case 3:
                return std::make_shared<LineFollowProd>(LineFollowProd());

            default:
                throw new NoTaskFoundException(id);
        }
    }

};


#endif //IMAGEPROCESSOR_TASK_H
