//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_TASKINTEFACE_H
#define IMAGEPROCESSOR_TASKINTEFACE_H

class TaskPrcessor;
struct TaskProcessor;


#include <vector>
#include <opencv2/opencv.hpp>
#include "../Utils.h"
#include "../model/Run.h"


class TaskInterface {
public:
    /**
     * Interface for task definition
     * @param coordinates - vector of coordinates captured by the system
     * @param referenceSize - calculated distance between two coordinate points
     */
    virtual Utils::TaskAssessment process(std::shared_ptr<Run> run, double referenceSize) = 0;

    /**
     * Function that gets called by the main processor to let task decide if the run should be finished.
     * @param frame - current frame decoded into actors
     * @param image - raw frame
     * @param referenceSize - coordinate distance modifier
     * @return true, if the run should end
     */
    virtual bool isRunEndCondition(std::shared_ptr<Frame> frame, cv::Mat image, double referenceSize) = 0;

    /**
     * Gets the first robot ID from frame.
     * @param frame frame decoded into actors
     * @return id of the robot
     */
    int getRobotIDFromFrame(std::shared_ptr<Frame> frame);

    /**
     * Gets team id from frame
     * @param frame
     * @return
     */
    int getTeamIDFromFrame(std::shared_ptr<Frame> frame);
};



#endif //IMAGEPROCESSOR_TASKINTEFACE_H
