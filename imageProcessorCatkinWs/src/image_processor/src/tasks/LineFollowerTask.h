//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_LINEFOLLOWERTASK_H
#define IMAGEPROCESSOR_LINEFOLLOWERTASK_H


#include "TaskInterface.h"
#include "../model/Run.h"


class TaskProcessor;
struct TaskAssesment;


class LineFollowerTask : public TaskInterface {
public:

    LineFollowerTask();

    Utils::TaskAssessment process(std::shared_ptr<Run> run, double referenceSize) override;

    bool isRunEndCondition(std::shared_ptr<Frame> frame, cv::Mat image,  double referenceSize) override;

private:
    static constexpr long finishWaypointID = 1;
    static constexpr double minDistanceToWaypoint = 100;
    std::shared_ptr<Location> robotLocationInLastFrame = nullptr;
    std::shared_ptr<Actor> finish = nullptr;

    int robotIDInThisRun;

    double distance;
};


#endif //IMAGEPROCESSOR_LINEFOLLOWERTASK_H
