//
// Created by raigo on 9.08.17.
//

#include <iostream>
#include <sstream>
#include <Poco/Net/HTTPClientSession.h>
#include <Poco/Net/HTTPRequest.h>
#include <Poco/Net/HTTPResponse.h>
#include <Poco/Net/NetException.h>
#include <ros/ros.h>
#include "HttpService.h"

std::string HttpService::sendPostRequest(std::string url, int port, std::string resource, std::string body) {

    Poco::Net::HTTPClientSession session(url, (Poco::UInt16) port);
    session.setKeepAlive(true);

    Poco::Net::HTTPRequest req(Poco::Net::HTTPRequest::HTTP_POST, resource, Poco::Net::HTTPMessage::HTTP_1_1);
    req.setContentType("application/json");
    req.setKeepAlive(true);

    req.setContentLength((int) body.length());
    try {
        std::ostream &requestStream = session.sendRequest(req);
        requestStream << body;

        //TODO Debug write
        //LOGGING THIS TAKES A LOT OF POWER! DO NOT LOG UNLESS ABSOLUTELY SURE!
        //req.write(std::cout);
        //ROS_DEBUG("%s", body.c_str());

        Poco::Net::HTTPResponse res;
        std::istream &iStr = session.receiveResponse(res);

        std::stringstream responseStream;
        responseStream << iStr.rdbuf();
        return responseStream.str();
    } catch (const Poco::Net::NetException& ex) {
        ROS_ERROR("Poco network exception occurred while sending POST message: %s\n%s", ex.what(), ex.message());
        return "NET ERROR!";

    } catch (const std::exception& ex) {
        ROS_ERROR("Unable to send POST request, error: %s", ex.what());
        return "NET ERROR!";
    }


}
