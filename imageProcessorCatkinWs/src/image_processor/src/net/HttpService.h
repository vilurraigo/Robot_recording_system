//
// Created by raigo on 9.08.17.
//

#ifndef CPP_HTTPSERVICE_H
#define CPP_HTTPSERVICE_H

#include <string>

class HttpService {
public:

    /**
     * Simple method to send a post request to a HTTP server
     * @param url           - server URL (without "HTTP://" or "HTTPS://")
     * @param port          - server port
     * @param resource      - resource address on the server
     * @param body          - request body (JSON string)
     * @return response body as a string.
     */
    std::string sendPostRequest(std::string url, int port, std::string resource, std::string body);
};


#endif //CPP_HTTPSERVICE_H
