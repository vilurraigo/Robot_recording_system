//
// Created by raigo on 17.08.17.
//

#ifndef IMAGE_PROCESSOR_JSONSERIALISER_H
#define IMAGE_PROCESSOR_JSONSERIALISER_H


#include <string>

/**
 * TODO his class is a placeholder until a proper JSON framework is added to the project,
 * Right now this will do and I cannot be bothered to mess with CMAKE at the moment.
 */
class JsonSerialiser {

private:
    static std::string quote(std::string str);

    static std::string addFieldToJson(std::string key, std::string value);

    static std::string addFieldToJson(std::string key, int value);

public:
    static std::string composeImageRequestBody(int width, int height, std::string b64Img);
};


#endif //IMAGE_PROCESSOR_JSONSERIALISER_H
