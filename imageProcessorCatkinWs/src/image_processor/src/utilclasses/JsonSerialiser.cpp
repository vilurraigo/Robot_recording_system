//
// Created by raigo on 17.08.17.
//

#include "JsonSerialiser.h"
#include <string>

std::string JsonSerialiser::quote(std::string str) {
    return "\"" + str + "\"";
}

std::string JsonSerialiser::addFieldToJson(std::string key, std::string value) {
    return  " " + quote(key) + ":" + quote(value);
}

std::string JsonSerialiser::addFieldToJson(std::string key, int value) {
    return " " + quote(key) + ":" + std::to_string(value);
}

std::string JsonSerialiser::composeImageRequestBody(int width, int height, std::string b64Img) {
    return "{"
           + addFieldToJson("image", b64Img) + ",\n"
           + addFieldToJson("width", width) + ",\n"
           + addFieldToJson("height", height)
           + "}";
}