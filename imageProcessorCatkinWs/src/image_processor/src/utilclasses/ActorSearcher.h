//
// Created by raigo on 17.08.17.
//

#ifndef IMAGE_PROCESSOR_ACTORSEARCHER_H
#define IMAGE_PROCESSOR_ACTORSEARCHER_H


#include <memory>
#include <vector>
#include "../model/Actor.h"
#include "../model/Frame.h"

class ActorSearcher {
public:
    static std::shared_ptr<Actor> getFirstActorFromFrame(std::shared_ptr<Frame> &frame, ActorType actorType);
    static std::vector<std::shared_ptr<Actor>> getAllActorsFromFrame(std::shared_ptr<Frame> &frame, ActorType actorType);
    static std::shared_ptr<Actor> getFirstActorFromFrame(std::shared_ptr<Frame> &frame, ActorType actorType, int ID);
    static bool oneOfActorsIsWithinDistanceToTarget(std::shared_ptr<Actor> &target, std::vector<std::shared_ptr<Actor>> &actors, double distance);
    static ActorPtr getActorClosestToTarget(ActorPtr &target, ActorVector actors);
};


#endif //IMAGE_PROCESSOR_ACTORSEARCHER_H
