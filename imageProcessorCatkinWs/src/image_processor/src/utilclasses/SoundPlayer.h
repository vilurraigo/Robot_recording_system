//
// Created by raigo on 31.08.17.
//

#ifndef IMAGE_PROCESSOR_SOUNDPLAYER_H
#define IMAGE_PROCESSOR_SOUNDPLAYER_H


class SoundPlayer {

public:

    enum class Sound {
        SOUND_BELL
    };

    static void play(SoundPlayer::Sound soundToPlay);

};


#endif //IMAGE_PROCESSOR_SOUNDPLAYER_H
