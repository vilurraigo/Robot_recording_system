//
// Created by raigo on 17.08.17.
//

#include "ActorSearcher.h"
#include "../RuntimeVariables.h"
#include <ros/ros.h>

std::shared_ptr<Actor> ActorSearcher::getFirstActorFromFrame(std::shared_ptr<Frame> &frame, ActorType actorType) {
    for (std::shared_ptr<Actor> &actor : frame->getActorsInFrame()) {
        if (actor->getType() == actorType) {
            return actor;
        }
    }
    return nullptr;
}

std::shared_ptr<Actor> ActorSearcher::getFirstActorFromFrame(std::shared_ptr<Frame> &frame, ActorType actorType, int ID) {
    for (std::shared_ptr<Actor> &actor : frame->getActorsInFrame()) {
        if (actor->getType() == actorType && actor->getID() == ID) {
            return actor;
        }
    }
    return nullptr;
}

std::vector<std::shared_ptr<Actor>> ActorSearcher::getAllActorsFromFrame(std::shared_ptr<Frame> &frame, ActorType actorType) {
    std::vector<std::shared_ptr<Actor>> actors;

    for (std::shared_ptr<Actor> &actor : frame->getActorsInFrame()) {
        if (actor->getType() == actorType) {
            actors.emplace_back(actor);
        }
    }

    return actors;
}

bool ActorSearcher::oneOfActorsIsWithinDistanceToTarget(std::shared_ptr<Actor> &target,
                                                        std::vector<std::shared_ptr<Actor>> &actors, double distance) {
    for (std::shared_ptr<Actor> &actor : actors) {
        if (Utils::calculateDistanceBetween(target->getX(), target->getY(), actor->getX(), actor->getY(), RuntimeVariables::coordinateReferenceSize) <= distance) {
            return true;
        }
    }
    return false;
}

ActorPtr ActorSearcher::getActorClosestToTarget(ActorPtr &target, ActorVector actors) {
    double closestDistance = 1000000;
    ActorPtr closest = nullptr;


    for (ActorPtr &actor : actors) {
        double currentDistance = Utils::calculateDistanceBetween(target->getX(), target->getY(), actor->getX(), actor->getY(), RuntimeVariables::coordinateReferenceSize);

        if (currentDistance < closestDistance || closest == nullptr) {
            closest = actor;
            closestDistance = currentDistance;
        }
    }

    return closest;
}
