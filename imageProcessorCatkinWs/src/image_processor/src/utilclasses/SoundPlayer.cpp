//
// Created by raigo on 31.08.17.
//

#include <cstdlib>
#include <ros/ros.h>
#include "SoundPlayer.h"

void SoundPlayer::play(SoundPlayer::Sound soundToPlay) {

    if (soundToPlay == Sound::SOUND_BELL) {
        system("mpg123 ~/Sounds/bell.mp3 &");
    } else {
        ROS_ERROR("Sound player: Enum variable not connected to an audio file!");
    }

}
