//
// Modified version of code fount at https://stackoverflow.com/questions/180947/base64-decode-snippet-in-c
// Author: LihO
// Modified by Raigo Vilur
//

#ifndef CPP_BASE64_H
#define CPP_BASE64_H

#include <vector>
#include <string>

typedef unsigned char BYTE;

class Base64 {

public:
    /**
     * Encodes BYTE array as a BASE64 string
     * @param buf - byte array
     * @param bufLen - array length
     * @return Base64 std::string
     */
    static std::string base64_encode(BYTE const *buf, size_t bufLen);

    /**
     * Decodes base 64 string into a byte array
     * @param encoded_string - base64 encoded bytes
     * @return byte array
     */
    static std::vector<BYTE> base64_decode(std::string const& encoded_string);

    static bool is_base64(BYTE c);

private:
    static const std::string BASE64_CHARS;

};


#endif //CPP_BASE64_H
