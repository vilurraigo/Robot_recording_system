//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_NOTASKFOUNDEXCEPTION_H
#define IMAGEPROCESSOR_NOTASKFOUNDEXCEPTION_H


#include <exception>
#include <string>

class NoTaskFoundException : public std::exception {
protected:
    std::string message;

public:
    NoTaskFoundException(long id) {
        message = "No task found with id " + std::to_string(id) + "!";
    }


    const char *what() const throw() override {
        return message.c_str();
    }


};


#endif //IMAGEPROCESSOR_NOTASKFOUNDEXCEPTION_H
