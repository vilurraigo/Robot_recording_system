//
// Created by raigo on 16.03.17.
//

#ifndef IMAGEPROCESSOR_IMAGEREADEXCEPTION_H
#define IMAGEPROCESSOR_IMAGEREADEXCEPTION_H


#include <exception>
#include <string>

class ImageReadException : public std::exception {
protected:
    std::string message;

public:
    ImageReadException() {
        message = "Unable to get image from camera!";
    }


    const char *what() const throw() override {
        return message.c_str();
    }

};


#endif //IMAGEPROCESSOR_IMAGEREADEXCEPTION_H
