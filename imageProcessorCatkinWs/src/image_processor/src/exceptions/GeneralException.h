//
// Created by raigo on 17.08.17.
//

#ifndef IMAGE_PROCESSOR_GENERALEXCEPTION_H
#define IMAGE_PROCESSOR_GENERALEXCEPTION_H

#include <exception>
#include <string>

class GeneralException : public std::exception {
protected:
    std::exception child;

public:
    GeneralException(std::exception& e) {
        child = e;
    }


    const char *what() const throw() override {
        return child.what();
    }

};

#endif //IMAGE_PROCESSOR_GENERALEXCEPTION_H
