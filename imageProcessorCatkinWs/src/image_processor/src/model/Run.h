//
// Created by raigo on 18.03.17.
//

#ifndef IMAGEPROCESSOR_RUN_H
#define IMAGEPROCESSOR_RUN_H


#include <vector>
#include "Frame.h"

class Run {

private:

    std::vector<std::shared_ptr<Frame>> framesInRun;
    int robotID;
    long runID;
    int taskID;
    int teamID;
    long currentImageID;
    int frameRate;

public:

    int getFrameRate() const {
        return frameRate;
    }

    void setFrameRate(int frameRate) {
        Run::frameRate = frameRate;
    }

    void addFrameToRun(std::shared_ptr<Frame> frame) {
        framesInRun.push_back(frame);
    }

    std::vector<std::shared_ptr<Frame>> getFrames() {
        return framesInRun;
    }

    int getRobotID() const {
        return robotID;
    }

    void setRobotID(int robotID) {
        this->robotID = robotID;
    }

    long getRunID() const {
        return runID;
    }

    void setRunID(long runID) {
        this->runID = runID;
    }

    int getTaskID() const {
        return taskID;
    }

    void setTaskID(int taskID) {
        this->taskID = taskID;
    }

    long getCurrentImageID() const {
        return currentImageID;
    }

    void setCurrentImageID(long currentImageID) {
        this->currentImageID = currentImageID;
    }

    int getTeamID() const {
        return teamID;
    }

    void setTeamID(int teamID) {
        Run::teamID = teamID;
    }

    void incrementImageID() {
        this->currentImageID++;
    }

};


#endif //IMAGEPROCESSOR_RUN_H
