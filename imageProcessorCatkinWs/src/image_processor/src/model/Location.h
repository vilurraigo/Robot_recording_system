//
// Created by raigo on 13.07.17.
//

#ifndef IMAGE_PROCESSOR_LOCATION_H
#define IMAGE_PROCESSOR_LOCATION_H

#include <memory>

class Location {

private:
    double x;
    double y;
    double rotation;

public:
    Location(double x, double y, double rotation);

    double getX() const;

    void setX(double x);

    double getY() const;

    void setY(double y);

    double getRotation() const;

    void setRotation(double rotation);

};
typedef std::shared_ptr<Location> LocationPtr;

#endif //IMAGE_PROCESSOR_LOCATION_H
