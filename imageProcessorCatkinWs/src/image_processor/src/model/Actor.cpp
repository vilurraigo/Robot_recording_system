//
// Created by raigo on 15.03.17.
//

#include "Actor.h"

double Actor::getX() {
    return location->getX();
}

double Actor::getY() {
    return location->getY();
}

double Actor::getRotation() {
    return location->getRotation();
}

ActorType Actor::getType() {
    return type;
}

int Actor::getID() {
    return ID;
}

Actor::Actor(int ID, int tagID, double x, double y, double rotation, ActorType type) {
    location = std::make_shared<Location>(Location(x, y, rotation));
    this->type = type;
    this->ID = ID;
    this->tagID = tagID;
}

Actor::Actor(int ID, int tagID, std::shared_ptr<Location> location, ActorType type) {
    this->location = location;
    this->type = type;
    this->ID = ID;
    this->tagID = tagID;
}

std::string Actor::toString() {
    return "Actor [" + std::to_string(tagID) + "/" + std::to_string(ID) + "] at (" + std::to_string(getX()) + ", " + std::to_string(getY()) + " rot = "+ std::to_string(
            getRotation())+")";
}

const std::shared_ptr<Location> &Actor::getLocation() const {
    return location;
}
