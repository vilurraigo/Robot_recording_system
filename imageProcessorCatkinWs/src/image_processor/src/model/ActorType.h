//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_TASKOBJECTS_H
#define IMAGEPROCESSOR_TASKOBJECTS_H

/**
 * Lists all possible actor types.
 */
enum class ActorType {

    ROBOT,
    TASK_SIGN,
    WAYPOINT,
    TEAM
};


#endif //IMAGEPROCESSOR_TASKOBJECTS_H
