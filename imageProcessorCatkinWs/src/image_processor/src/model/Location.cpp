//
// Created by raigo on 13.07.17.
//

#include "Location.h"

double Location::getX() const {
    return x;
}

void Location::setX(double x) {
    Location::x = x;
}

double Location::getY() const {
    return y;
}

void Location::setY(double y) {
    Location::y = y;
}

double Location::getRotation() const {
    return rotation;
}

void Location::setRotation(double rotation) {
    Location::rotation = rotation;
}

Location::Location(double x, double y, double rotation) : x(x), y(y), rotation(rotation) {}
