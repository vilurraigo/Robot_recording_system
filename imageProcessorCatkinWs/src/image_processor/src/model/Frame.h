//
// Created by raigo on 18.03.17.
//

#ifndef IMAGEPROCESSOR_FRAME_H
#define IMAGEPROCESSOR_FRAME_H


#include <vector>
#include <memory>
#include <chrono>
#include "Actor.h"

typedef std::vector<ActorPtr> ActorVector;

class Frame {
private:
    ActorVector actorsInFrame;
    std::shared_ptr<std::chrono::system_clock::time_point> timestamp;


public:

    Frame();

    std::shared_ptr<std::chrono::system_clock::time_point> getTimeStamp();

    void addActorToFrame(std::shared_ptr<Actor> actor) {
        actorsInFrame.push_back(actor);
    }

    std::vector<std::shared_ptr<Actor>> getActorsInFrame() {
        return actorsInFrame;
    }

    ActorPtr getFirstActorByType(ActorType actorType);
    ActorPtr getFirstActorByType(ActorType actorType, int id);
};


#endif //IMAGEPROCESSOR_FRAME_H
