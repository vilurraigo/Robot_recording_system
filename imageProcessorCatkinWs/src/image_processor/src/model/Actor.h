//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_ACTOR_H
#define IMAGEPROCESSOR_ACTOR_H

#include <memory>
#include "../Utils.h"
#include "ActorType.h"
#include "Location.h"


class Actor {

private:
    std::shared_ptr<Location> location;
    ActorType type;
    int ID;
    int tagID;


public:

    Actor(int ID, int tagID, double x, double y, double rotation, ActorType type);
    Actor(int ID, int tagID, std::shared_ptr<Location> location, ActorType type);

    int getID();
    double getX();
    double getY();
    double getRotation();
    ActorType getType();

    std::string toString();

    const std::shared_ptr<Location> &getLocation() const;

};

typedef std::shared_ptr<Actor> ActorPtr;

#endif //IMAGEPROCESSOR_ACTOR_H
