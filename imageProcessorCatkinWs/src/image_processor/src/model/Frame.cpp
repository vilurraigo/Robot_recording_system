//
// Created by raigo on 18.03.17.
//

#include "Frame.h"

Frame::Frame() {
    timestamp = std::make_shared<std::chrono::system_clock::time_point>(std::chrono::system_clock::now());
}

std::shared_ptr<std::chrono::system_clock::time_point> Frame::getTimeStamp() {
    return timestamp;
}

std::shared_ptr<Actor> Frame::getFirstActorByType(ActorType actorType, int id) {
    for (std::shared_ptr<Actor> actor : actorsInFrame) {
        if (actor->getType() == actorType && (actor->getID() == id || id == -1)) {
            return actor;
        }
    }
    return nullptr;
}

std::shared_ptr<Actor> Frame::getFirstActorByType(ActorType actorType) {
    return getFirstActorByType(actorType, -1);
}
