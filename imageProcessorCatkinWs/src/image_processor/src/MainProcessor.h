//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_MAINPROCESSOR_H
#define IMAGEPROCESSOR_MAINPROCESSOR_H


#include <image_transport/image_transport.h>
#include "ImageProcessor/ImageProcessor.h"
#include "RuntimeVariables.h"
#include "model/Run.h"
#include "tasks/TaskInterface.h"
#include "database/DatabaseService.h"
#include <memory>

class MainProcessor {

public:

    MainProcessor();

    static constexpr int START_WAYPOINT_ID = 0;


    /**
     * Does one step of the program (processes one frame)
     */
    void step(const sensor_msgs::ImageConstPtr& msg);

private:

    //Members
    std::shared_ptr<ImageProcessor> imageProcessor;
    std::shared_ptr<Run> thisRun;
    std::shared_ptr<TaskInterface> currentTask;
    std::shared_ptr<DatabaseService> databaseService;
    std::shared_ptr<Actor> lastKnownStartLocation;

    int lastKnownTeamID = -1;
    int emptyFrameCounter = 0;

    int currentTaskId = -1;

    bool startWayPointHasBeenCovered;

    //Private methods

    /**
     * Converts a ROS sensormessage image to OpenCV image
     * @param msg ROS image
     * @return openCV cv::Mat image
     */
    cv::Mat convertImageMessageToOpenCV(const sensor_msgs::ImageConstPtr& msg);

    /**
     * Contains logic for task changes
     * @param frameObj - current frame
     */
    void processTaskChange(std::shared_ptr<Frame> frameObj);

    void processTeamChange(std::shared_ptr<Frame> &frameObj);

    /**
     * Process current frame of current run;
     * @param frame decoded image data
     * @param image raw image
     */
    void processRun(std::shared_ptr<Frame> frame, cv::Mat image);

    /**
     * Decides if run should be started
     * @param frame - current frame object
     * @return true, if run should be started. Otherwise false;
     */
    bool processRunStart(std::shared_ptr<Frame> frame);
    void endRun();

};


#endif //IMAGEPROCESSOR_MAINPROCESSOR_H
