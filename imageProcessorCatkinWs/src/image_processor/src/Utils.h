//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_UTILS_H
#define IMAGEPROCESSOR_UTILS_H

#include <string>
#include <tgmath.h>
#include <Eigen/Core>
#include <chrono>

class Utils {
public:
#ifndef PI
    static constexpr double PI = 3.14159265358979323846;
#endif
    static constexpr double TWOPI = 2.0*PI;

    /**
     * Represents all data gathered by task processor
     */
    struct TaskAssessment {
        int score;
        double distance;
        std::string jsonData;
        long taskTime;
    };

    /**
     * Calculates distance between two coordinate points
     * @param location1
     * @param location2
     * @param referenceSize
     * @return distance between points
     */
    static double calculateDistanceBetween(double x1, double y1, double x2, double y2, double referenceSize);

    /**
     * Converts rotation matrix to angles
     * @param wRo - rotation matrix
     * @param yaw
     * @param pitch
     * @param roll
     */
    static void wRo_to_euler(const Eigen::Matrix3d& wRo, double& yaw, double& pitch, double& roll);

    /**
    * Normalize angle to be within the interval [-pi,pi].
    */
    static inline double standardRad(double t);

    static double radToDeg(double rad);
};


#endif //IMAGEPROCESSOR_UTILS_H
