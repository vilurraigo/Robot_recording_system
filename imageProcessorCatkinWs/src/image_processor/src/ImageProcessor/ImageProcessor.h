//
// Created by raigo on 15.03.17.
//

#ifndef IMAGEPROCESSOR_IMAGEPROCESSOR_H
#define IMAGEPROCESSOR_IMAGEPROCESSOR_H


#include <vector>
#include <opencv2/core/mat.hpp>
#include <opencv2/aruco.hpp>
#include <memory>
#include "../model/Actor.h"
#include "../model/Frame.h"


class ImageProcessor {

public:
    ImageProcessor();

    /**
     * Gets all the actors currently present in the image (by AprilTag);
     * @param frame - grayscale image of the current frame
     * @return vector of all the actors in current frame
     */
    std::shared_ptr<Frame> getActorsInFrame(cv::Mat& frame);

private:
    Eigen::Matrix3d F;

    static constexpr int ROBOT_TAG_START_ID = 0;
    static constexpr int ROBOT_TAG_END_ID = 9;
    static constexpr int TASK_TAG_START_ID = 10;
    static constexpr int TASK_TAG_END_ID = 59;
    static constexpr int WAYPOINT_START_ID = 60;
    static constexpr int WAYPOINT_END_ID = 149;
    static constexpr int TEAM_START_ID = 150;

    /**
     * Gets all actors from the frame
     * @param frame - image to process
     * @param frameObj - returned frame object
     */
    void getActorByTypeInFrame(cv::Mat& frame, std::shared_ptr<Frame> frameObj);

    cv::Ptr<cv::aruco::DetectorParameters> detectorParameters;

    /**
     * Converts AprilTags detection to an actor object
     * @param detection - apriltags detection
     * @return an instance of Actor
     */
    std::shared_ptr<Actor> convertDetectionToActor(int id, std::vector<cv::Point2f> &corners);

    /**
     * Converts tag ID to an ActorType and a ActorType ID
     * @param tagID - ID read from an AprilTag
     * @param localID - returned type ID
     * @return ActorType that the tag ID belongs to
     */
    ActorType getActorTypeAndLocalIDByTagID(int tagID, int &localID);

    /**
     * Converts aruco response that consists of four corners into one coordinate for the tag center and a rotation
     * in degrees (0 is towards positive y)
     * @param corners - list of tag corners, always starting from top left and going clockwise
     * @return Location object
     */
    std::shared_ptr<Location> convertCornersToLocation(std::vector<cv::Point2f> &corners);


};


#endif //IMAGEPROCESSOR_IMAGEPROCESSOR_H
