//
// Created by raigo on 15.03.17.
//

#include "ImageProcessor.h"
#include "../RuntimeVariables.h"
#include "../Utils.h"

std::shared_ptr<Frame> ImageProcessor::getActorsInFrame(cv::Mat& frame) {
    std::shared_ptr<Frame> frameObj = std::make_shared<Frame>(Frame());

    //All different types of Actors that can be in a frame, currently used different tags for different actors, but
    //by it is possible to implement id based actors.
    getActorByTypeInFrame(frame, frameObj);
    ROS_INFO("Found: %d actors", frameObj->getActorsInFrame().size());

    return frameObj;
}


std::shared_ptr<Actor> ImageProcessor::convertDetectionToActor(int id, std::vector<cv::Point2f> &corners) {

    int localID = 0;
    ActorType type = getActorTypeAndLocalIDByTagID(id, localID);

    std::shared_ptr<Actor> actor = std::make_shared<Actor>(Actor(localID, id, convertCornersToLocation(corners), type));
    std::cout << actor->toString() << std::endl;
    //TODO why this prints gibberish:
    ROS_INFO("Found %s", actor->toString().c_str());
    return actor;
}

void ImageProcessor::getActorByTypeInFrame(cv::Mat &frame, std::shared_ptr<Frame> frameObj) {
    std::vector<int> detectedMarkerIDs;
    std::vector<std::vector<cv::Point2f>> markerCorners, rejectedMarkers;

    cv::aruco::detectMarkers(frame, cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_1000), markerCorners, detectedMarkerIDs, this->detectorParameters, rejectedMarkers);
    ROS_INFO("Detections: %d ", detectedMarkerIDs.size());
    ROS_DEBUG("Discarded tags: %d", rejectedMarkers.size());
    auto  markerCornerIterator = markerCorners.begin();
    for (int detectedID : detectedMarkerIDs) {
        frameObj->addActorToFrame(convertDetectionToActor(detectedID, *markerCornerIterator));
        markerCornerIterator++;
    }
}

ImageProcessor::ImageProcessor() {
    F <<    1,   0,   0,
            0,  -1,   0,
            0,   0,   1;
    this->detectorParameters = cv::aruco::DetectorParameters::create();
}

ActorType ImageProcessor::getActorTypeAndLocalIDByTagID(int tagID, int &localID) {
    if (ROBOT_TAG_START_ID <= tagID && tagID <= ROBOT_TAG_END_ID) {
        localID = tagID - ROBOT_TAG_START_ID;
        return ActorType::ROBOT;
    } if (TASK_TAG_START_ID <= tagID && tagID <= TASK_TAG_END_ID) {
        localID = tagID - TASK_TAG_START_ID;
        return ActorType::TASK_SIGN;
    } if (WAYPOINT_START_ID <= tagID && tagID <= WAYPOINT_END_ID) {
        localID = tagID - WAYPOINT_START_ID;
        return ActorType::WAYPOINT;
    } else {
        localID = tagID - TEAM_START_ID;
        return ActorType::TEAM;
    }
}

std::shared_ptr<Location> ImageProcessor::convertCornersToLocation(std::vector<cv::Point2f> &corners) {

    cv::Point2f topLeft = corners.at(0);
    cv::Point2f topRight = corners.at(1);
    cv::Point2f bottomRight = corners.at(2);
    cv::Point2f bottomLeft = corners.at(3);



            double x = (topLeft.x - bottomRight.x) / 2 + topLeft.x;
    double y = (topLeft.y - bottomRight.y) / 2 + topLeft.y;


    double rotation = Utils::radToDeg(atan2(topLeft.y - bottomLeft.y, topLeft.x - bottomLeft.y));

    std::shared_ptr<Location> location = std::make_shared<Location>(Location(x, y, rotation));

    return location;
}
