//
// Created by raigo on 23.03.17.
//

#include "Utils.h"
#include <ctime>
#include <cmath>

void Utils::wRo_to_euler(const Eigen::Matrix3d& wRo, double& yaw, double& pitch, double& roll) {
    yaw = standardRad(atan2(wRo(1,0), wRo(0,0)));
    double c = cos(yaw);
    double s = sin(yaw);
    pitch = standardRad(atan2(-wRo(2,0), wRo(0,0)*c + wRo(1,0)*s));
    roll  = standardRad(atan2(wRo(0,2)*s - wRo(1,2)*c, -wRo(0,1)*s + wRo(1,1)*c));
}

double Utils::standardRad(double t) {
    if (t >= 0.) {
        t = fmod(t+PI, TWOPI) - PI;
    } else {
        t = fmod(t-PI, -TWOPI) + PI;
    }
    return t;
}

double Utils::calculateDistanceBetween(double x1, double y1, double x2, double y2, double referenceSize) {
    return std::sqrt(std::pow(x1 - x2, 2.0) + std::pow(y1 - y2, 2.0)) * referenceSize;
}

double Utils::radToDeg(double rad) {
    return rad * (180 / M_PI);
}
