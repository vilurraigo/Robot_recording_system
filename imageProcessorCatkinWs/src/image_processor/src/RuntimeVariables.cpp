//
// Created by raigo on 16.03.17.
//

#include "RuntimeVariables.h"

//Default variable values:
double RuntimeVariables::coordinateReferenceSize = 0.1;
std::string RuntimeVariables::imageTopic = "/kinect2/hd/image_color";
int RuntimeVariables::maxEmptyFrames = 5;
std::string RuntimeVariables::dbConnectionString = "";
int RuntimeVariables::imageServerPort = 80;
std::string RuntimeVariables::imageServer = "";
double RuntimeVariables::distanceFromTag = 10.0;

double RuntimeVariables::tag_size = 0.2;
double RuntimeVariables::fx = 1;
double RuntimeVariables::fy = 1;
double RuntimeVariables::px = 0.5;
double RuntimeVariables::py = 0.5;
int RuntimeVariables::frameRate = 10;

void RuntimeVariables::reconfigure(image_processor::image_processor_configConfig &config) {
    //TODO add frame rate to dynamic config
    imageTopic = config.image_topic;
    coordinateReferenceSize = config.coordinate_ref_size;
    maxEmptyFrames = config.maxEmptyFrames;
    distanceFromTag = config.distanceToTag;
    // dbConnectionString = config.dbConnectionString;
    fx = config.fx;
    fy = config.fy;
    px = config.px;
    py = config.py;
    //imageTransportString = config.imageTransportString;
}
