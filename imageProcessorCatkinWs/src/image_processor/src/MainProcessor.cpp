//
// Created by raigo on 15.03.17.
//

#include <opencv2/opencv.hpp>
#include <cv_bridge/cv_bridge.h>
#include <chrono>
#include <thread>
#include <vector>
#include "MainProcessor.h"
#include "tasks/TaskHolder.h"
#include "exceptions/GeneralException.h"
#include "utilclasses/ActorSearcher.h"
#include "utilclasses/SoundPlayer.h"


MainProcessor::MainProcessor() {
    imageProcessor = std::make_shared<ImageProcessor>(ImageProcessor());
    databaseService = std::make_shared<DatabaseService>(DatabaseService());
    emptyFrameCounter = 0;
    startWayPointHasBeenCovered = false;
    lastKnownStartLocation == nullptr;
    lastKnownTeamID = -1;
}

void MainProcessor::step(const sensor_msgs::ImageConstPtr &msg) {

    try {
        //Get image
        cv::Mat frame = convertImageMessageToOpenCV(msg);

        //Get actors from image
        std::shared_ptr<Frame> frameObj = imageProcessor->getActorsInFrame(frame);

        processTaskChange(frameObj);
        processTeamChange(frameObj);

        if (currentTaskId != -1) {
            ROS_DEBUG("Main: Using task %d", currentTaskId);
            processRun(frameObj, frame);
        } else {
            ROS_ERROR("Main: task is not defined, cannot start run!");
        }
    } catch (GeneralException &e) {
        ROS_ERROR("Main: Caught exception:\n%s", e.what());
        return;
    }

}

void MainProcessor::processRun(std::shared_ptr<Frame> frame, cv::Mat image) {

    if (thisRun == nullptr) {
        //No run currectly started, check if start conditions are met:
        if (!processRunStart(frame)) {
            return;
        }
    }

    ROS_DEBUG("Run in progress");

    std::shared_ptr<Actor> robotFromFrame = ActorSearcher::getFirstActorFromFrame(frame, ActorType::ROBOT);

    databaseService->saveImage(image, thisRun->getCurrentImageID(), thisRun->getRunID());
    thisRun->incrementImageID();

    //Deal with robot not present
    if (robotFromFrame == nullptr && emptyFrameCounter < RuntimeVariables::maxEmptyFrames) {
        //Robot is not here, but limit has not been reached
        emptyFrameCounter++;
        ROS_DEBUG("No robot found in camera view (%d/%d)", emptyFrameCounter, RuntimeVariables::maxEmptyFrames);
        return;
    } else if (robotFromFrame == nullptr && emptyFrameCounter >= RuntimeVariables::maxEmptyFrames) {
        //Robot is not here and limit has been reached:
        ROS_INFO("Run ended because robot was not seen for %d consecutive frames", RuntimeVariables::maxEmptyFrames);
        thisRun->addFrameToRun(frame);
        endRun();
        return;
    } else {
        //Robot is visible again
        emptyFrameCounter = 0;
        thisRun->addFrameToRun(frame);
    }

    //Run ends, if the end conditions for the task have been met
    if (currentTask->isRunEndCondition(frame, image, RuntimeVariables::coordinateReferenceSize)) {
        ROS_INFO("Run ended because task reported a run end condition");
        endRun();
        return;
    }

}

bool MainProcessor::processRunStart(std::shared_ptr<Frame> frame) {

    std::shared_ptr<Actor> startTagLocation = ActorSearcher::getFirstActorFromFrame(frame, ActorType::WAYPOINT, START_WAYPOINT_ID);

    if (startTagLocation == nullptr) {
        //Tag not found!

        if (lastKnownStartLocation == nullptr) {
            ROS_INFO("Main: Cannot start run, because start location is not known!");
            return false;
        } else {
            //At least one robot must be near the last known start tag location
            std::vector<std::shared_ptr<Actor>> robots = ActorSearcher::getAllActorsFromFrame(frame, ActorType::ROBOT);
            if (ActorSearcher::oneOfActorsIsWithinDistanceToTarget(lastKnownStartLocation, robots, RuntimeVariables::distanceFromTag)) {
                ROS_INFO("Main: Start tag covering recognised. Found robot ID=%d near the tag", 0);
                startWayPointHasBeenCovered = true;
                return false;
            } else {
                ROS_INFO("Main: Start tag not found in frame, but no robot seen near last known positon");
                return false;
            }

        }
    } else {
        //Tag found!
        lastKnownStartLocation = startTagLocation;
        if (startWayPointHasBeenCovered) {
            //Tag has been covered and is now visible again
            if (lastKnownTeamID == -1) {
                ROS_INFO("Main: cannot start run, team not known");
                return false;
            } else if (currentTaskId == -1) {
                ROS_INFO("Main: cannot start run, task not known");
                return false;
            }
            ROS_INFO("Main: Start tag released, starting new run!");
            thisRun = std::make_shared<Run>(Run());
            thisRun->setTaskID(currentTaskId);

            //TODO this robot ID is pointless:
            thisRun->setRobotID(currentTask->getRobotIDFromFrame(frame));
            thisRun->setTeamID(lastKnownTeamID);
            thisRun->setRunID(databaseService->requestRunId());
            thisRun->setFrameRate(RuntimeVariables::frameRate);
            SoundPlayer::play(SoundPlayer::Sound::SOUND_BELL);

            startWayPointHasBeenCovered = false;
            return true;
        } else {
            //This means that task is visible and has not been covered
            return false;
        }
    }
}

void MainProcessor::endRun() {

    //Let task assessor calculate final score and data
    Utils::TaskAssessment assessment =
            currentTask->process(thisRun, RuntimeVariables::coordinateReferenceSize);

    databaseService->saveRun(thisRun, assessment);

    //Reset run data;
    thisRun = nullptr;
    emptyFrameCounter = 0;

    //Signal run end
    SoundPlayer::play(SoundPlayer::Sound::SOUND_BELL);

}

cv::Mat MainProcessor::convertImageMessageToOpenCV(const sensor_msgs::ImageConstPtr &msg) {
    try {
        cv::Mat frame = cv_bridge::toCvShare(msg, "bgr8")->image;
        ROS_DEBUG("Image converted");
        return frame;
    } catch (cv_bridge::Exception &e) {
        ROS_ERROR("Could not convert image from '%s' to 'bgr8'.", msg->encoding.c_str());
        throw new GeneralException(e);
    }
}

void MainProcessor::processTaskChange(std::shared_ptr<Frame> frameObj) {

    std::vector<std::shared_ptr<Actor>> tasks = ActorSearcher::getAllActorsFromFrame(frameObj, ActorType::TASK_SIGN);


    if (tasks.empty()) {
        ROS_INFO("Main: No task sign found, using current task: %d", currentTaskId);
        return;
    }

    int thisFrameTaskId = tasks.front()->getID();

    if (tasks.size() > 1) {
        ROS_INFO("Main: Found two or more task signs in frame, using first one found in this frame: %d", thisFrameTaskId);
    }

    if (currentTaskId != thisFrameTaskId) {
        /*
        In order to not brake current runs if the task sign is hidden from view, task changes are only triggered when
        new tag is in the view.
        */
        ROS_INFO("Changed task to %d. Old task %d", thisFrameTaskId, currentTaskId);
        currentTask = TaskHolder::getTaskById(thisFrameTaskId);
        currentTaskId = thisFrameTaskId;
    }
}

void MainProcessor::processTeamChange(std::shared_ptr<Frame> &frameObj) {

    std::vector<std::shared_ptr<Actor>> teamTags = ActorSearcher::getAllActorsFromFrame(frameObj, ActorType::TEAM);


    if (teamTags.empty()) {
        ROS_DEBUG("Main: No team tag found, using current team: %d", lastKnownTeamID);
        return;
    }

    int currentFrameTeamID = teamTags.front()->getID();

    if (currentFrameTeamID != lastKnownTeamID) {
        lastKnownTeamID = currentFrameTeamID;
        if (teamTags.size() > 1) {
            //TODO first choice should be the old team
            ROS_INFO("Main: Found two or more team signs in frame, using first one found in this frame: %d", lastKnownTeamID);
        } else {
            ROS_INFO("Main: Changing team to %d", lastKnownTeamID);
        }
    } else {
        ROS_DEBUG("Main: team not changed");
    }
}
