#include "ros/ros.h"
#include "src/MainProcessor.h"
#include "src/RuntimeVariables.h"
#include <image_transport/image_transport.h>
#include <ros/console.h>

#include <dynamic_reconfigure/server.h>
#include <image_processor/image_processor_configConfig.h>
#include <boost/bind.hpp>
#include <boost/function.hpp>
#include <string>


ros::Subscriber sub;

void reconfigureCallback(image_processor::image_processor_configConfig &config, uint32_t level) {
    ROS_INFO("Config changed");
    RuntimeVariables::reconfigure(config);
}

int main(int argc, char **argv) {

    if( ros::console::set_logger_level(ROSCONSOLE_DEFAULT_NAME, ros::console::levels::Debug) ) {
        ros::console::notifyLoggerLevelsChanged();
    }

    ROS_INFO("Image processor started");

    ros::init(argc, argv, "imageProcessor");
    ros::NodeHandle n("~");
    n.param("db", RuntimeVariables::dbConnectionString, std::string());
    n.param("image_server", RuntimeVariables::imageServer, std::string());
    n.param("image_server_port", RuntimeVariables::imageServerPort, int());

    ROS_INFO("Db connection string: %s", RuntimeVariables::dbConnectionString.c_str());
    std::string imageTransportAddress = RuntimeVariables::imageServer + ":" + std::to_string(RuntimeVariables::imageServerPort);
    ROS_INFO("Address used for image saving: %s", imageTransportAddress.c_str());



    MainProcessor mainProcessor = MainProcessor();



    dynamic_reconfigure::Server<image_processor::image_processor_configConfig> server;
    dynamic_reconfigure::Server<image_processor::image_processor_configConfig>::CallbackType callBackType;
    callBackType = boost::bind(&reconfigureCallback, _1, _2);

    server.setCallback(callBackType);

    sub = n.subscribe(RuntimeVariables::imageTopic, 1, &MainProcessor::step, &mainProcessor);

    ros::spin();

    return 0;
}
