#!/bin/sh

#this is a deploy script for deploying image processor on Odroid

ssh odroid@192.168.112.29 "./delete-img-processor-dir.sh"
scp -r src/image_processor odroid@192.168.112.29:~/catkin_ws/src/
ssh odroid@192.168.112.29 "./copy-lib.sh"

