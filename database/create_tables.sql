DROP TABLE IF EXISTS team CASCADE;
CREATE TABLE team (
  team_id SERIAL PRIMARY KEY,
  team_name VARCHAR(256) UNIQUE NOT NULL,
  tag_id INTEGER UNIQUE,
  UNIQUE (tag_id)
);

DROP TABLE IF EXISTS task CASCADE;
CREATE TABLE task (
  task_id INTEGER PRIMARY KEY,
  task_name VARCHAR(256) UNIQUE
);

DROP TABLE IF EXISTS student CASCADE;
CREATE TABLE student (
  student_id VARCHAR(10),
  student_name VARCHAR(500),
  PRIMARY KEY(student_id)
);

DROP TABLE IF EXISTS student_team CASCADE;
CREATE TABLE student_team (
  student_id VARCHAR(10) NOT NULL,
  team_id INTEGER NOT NULL,
  PRIMARY KEY(student_id, team_id),
  FOREIGN KEY(team_id) REFERENCES team(team_id),
  FOREIGN KEY(student_id) REFERENCES student(student_id)
);

DROP TABLE IF EXISTS run CASCADE;
CREATE TABLE run (
  run_id BIGINT PRIMARY KEY,
  task_id INTEGER NOT NULL,
  team_id INTEGER,
  framerate INTEGER NOT NULL,
  run_time TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  last_frame_id INTEGER NOT NULL,
  score INTEGER,
  ms_taken BIGINT,
  run_data JSON,
  FOREIGN KEY(team_id) REFERENCES team(team_id),
  FOREIGN KEY(task_id) REFERENCES task(task_id)
);

DROP TABLE IF EXISTS admin CASCADE;
CREATE TABLE admin (
  admin_id SERIAL PRIMARY KEY,
  username VARCHAR(256),
  salt VARCHAR(50),
  password_hash VARCHAR(256)
);
