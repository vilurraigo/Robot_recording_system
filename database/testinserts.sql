

INSERT INTO task(task_id, task_name) VALUES (0, 'Line Following');


INSERT INTO admin(username, salt, password_hash) VALUES ('admin',
 'rc8fMdQFN0xqMRAP9Lm+S4N2M1E=',
 'c2NyeXB0ABEAAAAIAAAAAeqMDdGfmsR4cnv565jPUCSCWb7fEdfJT7wUbomsSD34vDFNt927FCVUZT0bIl9BKiGfmcnIbSdE37xiUG9NBBbKU51aeBCXWlYYkD+qbo+V');

INSERT INTO team(team_name, tag_id) VALUES ('heelium', 0);
INSERT INTO team(team_name, tag_id) VALUES ('vesinik', 1);

--This run has to have ID of 0, because it has test image data.
INSERT INTO run(run_id, team_id, task_id, framerate, score, run_data, last_frame_id) VALUES (0, (SELECT team_id FROM team WHERE tag_id = 0), (SELECT task_id FROM task WHERE task_name = 'Line Following') , 20, 123, '{}', 99);

INSERT INTO run(run_id, team_id, task_id, framerate, score, run_data, last_frame_id) VALUES (NEXTVAL('run_id_seq'), NULL, (SELECT task_id FROM task WHERE task_name = 'Line Following') , 20, 123, '{}', 0);
