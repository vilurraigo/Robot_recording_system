
--Sequence for getting run id-s
DROP SEQUENCE IF EXISTS run_id_seq;
CREATE SEQUENCE run_id_seq;


-- This function makes it possible for image-processor to insert runs to teams without actually knowing what team is currently running
-- If no team is found NULL is returned and the run should be saved with that team_id of NULL to show a teamless run (made outside active hours).
DROP FUNCTION IF EXISTS get_team_by_day_time_and_robot(INTEGER, TIME, INTEGER);
CREATE FUNCTION get_team_by_day_time_and_robot(_day INTEGER, _time TIME, _robot_id INTEGER) RETURNS INTEGER AS $$
  SELECT team_id FROM team WHERE robot_id = _robot_id AND practice_id =
  (SELECT practice_id FROM practice WHERE practice_day_int = _day AND _time > practice_start_time AND _time < practice_end_time)
$$ LANGUAGE SQL;


DROP FUNCTION IF EXISTS get_score(VARCHAR(10), INTEGER);

CREATE FUNCTION get_score(_student_id VARCHAR(10), _task_id INTEGER) RETURNS INTEGER AS $$
  SELECT score FROM run WHERE team_id = (select team_id from student_team where student_id = _student_id) AND task_id = _task_id ORDER BY score DESC LIMIT 1
$$ LANGUAGE SQL;
