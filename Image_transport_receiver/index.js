var Jimp = require("jimp");
var express = require('express');
var app = express();
var bodyParser = require('body-parser');

app.use(bodyParser.json({limit: '50mb'}));

app.post("/img/:runId/:imgId", (req, res) => {

  console.log("Got image:");
  console.log("Image ID:   " + req.params.imgId);
  console.log("Run ID:     " + req.params.runId);
  console.log("Image size :" + req.body.width + " by " + req.body.height);
  var image = new Jimp(parseInt(req.body.width), parseInt(req.body.height));
  image.bitmap.data = Buffer.from(req.body.image, 'base64');

  image.resize(Jimp.AUTO, 360);

  image.write("img/" + req.params.runId + "/image"+ req.params.imgId +".jpg");

  res.send({success: "true"});
});

console.log("Server started on port 3000");
app.listen(3000);
