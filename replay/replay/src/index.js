import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory } from 'react-router';
import { LocaleProvider } from 'antd';
import enUS from 'antd/lib/locale-provider/en_US';

import Routes from './routes';

import './index.css';

ReactDOM.render(
  <div className="outer_container">
  <LocaleProvider locale={enUS}>
      <Routes history={browserHistory} />
  </LocaleProvider>
  </div>,
  document.getElementById('root')
);
