const scrypt = require("scrypt");
const crypto = require("crypto");


function AuthConfig() {
    this.scryptParameters = {};

    this.setup = function () {
        try {
          //Uses 0.5 for maxtime, and default values maxmem and maxmemfrac
          this.scryptParameters = scrypt.paramsSync(0.5);
          console.log(this.scryptParameters);
        } catch(err) {
          console.log(err);
          process.exit()
        }
    };


    this.hash = function (password) {
          var salt = crypto.randomBytes(20).toString('base64');
          var key = new Buffer(salt + password);
          var passwordHash = scrypt.kdfSync(key, this.scryptParameters).toString('base64');
          return {
            salt: salt,
            passwordHash: passwordHash
          };
    };

    this.verify = function (password, salt, passwordHashInBase64) {
          var passwordHash = new Buffer(passwordHashInBase64, 'base64');
          var key = new Buffer(salt + password);
          return scrypt.verifyKdfSync(passwordHash, key);
    };
};

module.exports = new AuthConfig();
