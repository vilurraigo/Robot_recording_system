const express = require('express');
const morgan = require('morgan');
const path = require('path');
const bodyParser = require('body-parser');
const models = require('./models');
const jwt = require('jsonwebtoken');
var cors = require('cors');
var helmet = require('helmet')

const scrypt = require("scrypt");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
// Setup logger
app.use(morgan(':remote-addr - :remote-user [:date[clf]] ":method :url HTTP/:http-version" :status :res[content-length] :response-time ms'));

app.use(helmet());
app.use(helmet.contentSecurityPolicy({
  directives: {
    defaultSrc: ["'self'", "data:", "blob:"],
    scriptSrc: ["'self'", "'unsafe-inline'"],
    styleSrc: ["'self'", 'maxcdn.bootstrapcdn.com'],
    imgSrc: ["'self'", 'data:']
  },
  loose: true,
  setAllHeaders: true,
  browserSniff: true
}));

const auth = require('./auth/auth.js');

auth.setup();


/*
//If database password gets lost, uncomment this to generate a new one. Change "pass" to whatever you like.
var hashResult = auth.hash("pass");
console.log(hashResult);
console.log("Validation: " + auth.verify("pass", hashResult.salt, hashResult.passwordHash));
*/


app.use(cors());

app.post('/api/v1/authenticate', (req, res) => {
  if (!req.body.username) {
    res.json({ success: false, message: 'Authentication failed. User not found.' });
  }
  console.log("Trying to log in as: " + req.body.username);
  models.sequelize.query("SELECT * FROM admin WHERE username = $1", { bind: [req.body.username], type: models.sequelize.QueryTypes.SELECT}
  ).then((user) => {
    //removing unnecessary array wrap
    user = user[0];
    if (user) {
      console.log(user);
        if (!auth.verify(req.body.password, user.salt, user.password_hash)) {
          res.json({ success: false, message: 'Authentication failed. Wrong password.' });
        } else {
          //TODO remove hardcoded supersecret, should use private key
          var tokenizedUserData = {username: user.username}
          var token = jwt.sign(tokenizedUserData, 'superSecret');

          res.json({
            success: true,
            message: 'Enjoy your token!',
            token: token
          });
        }
    } else {
      res.json({ success: false, message: 'Authentication failed. User not found.' });
    }
  }).catch((reason) => {
      console.log(reason);
      res.json({ success: false, message: 'Authentication failed. User not found.' });
  });
});

app.get('/api/v1/team', (req, res) => {
  models.sequelize.query("SELECT * FROM team",
   {type: models.sequelize.QueryTypes.SELECT}
 ).then((teams) => {
      res.json(teams);
  }).catch((reason) => {
      res.json({ success: false, message: 'Getting teams failed: ' + reason });
  });
});

app.get('/api/v1/team/:teamID/run/', (req, res) => {
  models.sequelize.query("SELECT * FROM run r LEFT JOIN task t ON r.task_id = t.task_id LEFT JOIN team te ON r.team_id = te.team_id WHERE r.team_id = $1",
   { bind: [req.params.teamID], type: models.sequelize.QueryTypes.SELECT}
  ).then(teams => {
    res.json(teams);
  });
});

app.get('/api/v1/run/:runID', (req, res) => {
  models.sequelize.query("SELECT * FROM run r LEFT JOIN task t ON r.task_id = t.task_id LEFT JOIN team te ON r.team_id = te.team_id WHERE r.run_id = $1 ORDER BY r.run_time DESC",
   {bind: [req.params.runID], type: models.sequelize.QueryTypes.SELECT}
  ).then(run => {
    res.json(run[0]);
  });
});

app.get('/api/v1/run/', (req, res) => {
  models.sequelize.query("SELECT * FROM run r LEFT JOIN task t ON r.task_id = t.task_id LEFT JOIN team te ON r.team_id = te.team_id WHERE r.run_time BETWEEN $1 AND $2 ORDER BY r.run_time DESC",
   {bind: [req.query.lowerLimit, req.query.upperLimit], type: models.sequelize.QueryTypes.SELECT}
  ).then(runs => {
    res.json(runs);
  });
});


/*ADMIN ROUTES COME HERE*/
var adminRoutes = express.Router();

//Authentication middleware
adminRoutes.use(function(req, res, next) {

  var token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, 'superSecret', function(err, decoded) {
      if (err) {
        return res.json({ success: false, message: 'Failed to authenticate token.' });
      } else {

        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.status(403).send({
        success: false,
        message: 'No token provided.'
    });

  }
});

adminRoutes.get('/home', (req, res) => {
  res.json({ success: true, message: 'Successfully logged in' });
});

//Get students
adminRoutes.get('/student', (req, res) => {
  models.sequelize.query("SELECT * FROM student",
   {type: models.sequelize.QueryTypes.SELECT}
 ).then(students => {
    res.json(students);
  });
});

//Get student teams
adminRoutes.get('/student/:student_id/teams', (req, res) => {
  models.sequelize.query("SELECT team_id, team_name FROM team WHERE team_id IN (SELECT team_id FROM student_team WHERE student_id = $1)",
   {bind: [req.params.student_id], type: models.sequelize.QueryTypes.SELECT}
 ).then(student_teams => {
    res.json(student_teams);
  }).catch((reason) => {
      res.json({ success: false, message: 'Could not get student teams: ' + reason });
  });
});

//Add student
adminRoutes.post('/student', (req, res) => {
  models.sequelize.query("INSERT INTO student(student_id, student_name) VALUES ($1, $2)",
   {bind: [req.body.student_id, req.body.student_name], type: models.sequelize.QueryTypes.INSERT}
  ).then(() => {
    res.json({success: true});
  }).catch((reason) => {
      res.json({ success: false, message: 'Student insertion failed: ' + reason });
  });
});

//Delete student. Warning deletes from teams too!
adminRoutes.delete('/student/:student_id', (req, res) => {
  models.sequelize.query("DELETE FROM student WHERE student_id = $1",
   {bind: [req.params.student_id], type: models.sequelize.QueryTypes.DELETE}
  ).then(() => {
    res.json({success: true});
  }).catch((reason) => {
      res.json({ success: false, message: 'Student deletion failed: ' + reason });
  });
});

//Add student to team
adminRoutes.post('/team/:team_id/student/:student_id', (req, res) => {
  console.log(req.params);
  models.sequelize.query("INSERT INTO student_team (team_id, student_id) VALUES ($1, $2)",
   {bind: [req.params.team_id, req.params.student_id], type: models.sequelize.QueryTypes.INSERT}
  ).then(() => {
    res.json({success: true});
  }).catch((reason) => {
      res.json({ success: false, message: 'Student insertion to team failed: ' + reason });
  });
});

//Remove student from team
adminRoutes.delete('/team/:team_id/student/:student_id', (req, res) => {
  console.log(req.params);
  models.sequelize.query("DELETE FROM student_team WHERE student_id = $1 AND team_id = $2",
   {bind: [req.params.student_id, req.params.team_id], type: models.sequelize.QueryTypes.DELETE}
  ).then(() => {
    res.json({success: true});
  }).catch((reason) => {
      res.json({ success: false, message: 'Student deletion from team failed: ' + reason });
  });
});

//Add team
adminRoutes.put('/team', (req, res) => {
  models.sequelize.query("INSERT INTO team(team_name, tag_id) VALUES ($1, $2)",
   {bind: [req.body.team_name, req.body.tag_id], type: models.sequelize.QueryTypes.INSERT}
  ).then(() => {
    res.json({success: true});
  }).catch((reason) => {
      res.json({ success: false, message: 'Team insertion failed: ' + reason });
  });
});

//Update team
adminRoutes.post('/team/:team_id', (req, res) => {
  models.sequelize.query("UPDATE team SET tag_id = $1, team_name = $2 WHERE team_id = $3 ",
   {bind: [req.body.tag_id, req.body.team_name, req.params.team_id], type: models.sequelize.QueryTypes.UPDATE}
  ).then(() => {
    res.json({success: true});
  }).catch((reason) => {
      res.json({ success: false, message: 'Team insertion failed: ' + reason });
  });
});

//Remove team
//This is commented out, because it is not supposed to be a endpoint.
//Team deletion makes runs orphans
/*adminRoutes.put('/team', (req, res) => {
  models.sequelize.query("INSERT INTO team(team_name, tag_id) VALUES ($1, $2)",
   {bind: [req.params.team_name, req.params.tag_id], type: models.sequelize.QueryTypes.INSERT}
  ).then(() => {
    res.json({success: true});
  }).catch((reason) => {
      res.json({ success: false, message: 'Team insertion failed: ' + reason });
  });
});*/


//Change a run-s team
adminRoutes.post('/run/:run_id/team/:team_id', (req, res) => {
  models.sequelize.query("UPDATE run SET team_id = $1 WHERE run_id = $2",
      {bind: [req.params.team_id, req.params.run_id], type: models.sequelize.QueryTypes.UPDATE}
  ).then(() => {
    models.sequelize.query("SELECT team_name FROM team WHERE team_id = $1",
        {bind: [req.params.team_id], type: models.sequelize.QueryTypes.SELECT}).then(name => {
          res.json({ success: true, message: 'All is okay', team_name: name[0].team_name});
        }).catch((reason) => {
            res.json({ success: false, message: 'Setting team failed: ' + reason });
        });
  }).catch((reason) => {
      res.json({ success: false, message: 'Setting team failed: ' + reason });
  });
});

app.use('/api/v1/admin', adminRoutes);

//Images endpont
app.use('/api/v1/images', express.static(path.resolve(__dirname, '..', '..', "images")));

//React static files
app.use(express.static(path.resolve(__dirname, '..', '..', 'build')));

//Always return the main index.html, so react-router can render the route in the client
app.get('*', (req, res) => {
  res.sendFile(path.resolve(__dirname, '..', '..', 'build', 'index.html'));
});

module.exports = app;
