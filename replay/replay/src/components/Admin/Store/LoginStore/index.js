import { EventEmitter } from 'events';
import jwt_decode from 'jwt-decode';
import AppDispatcher from '../../AppDispatcher';

class LoginStore extends EventEmitter {

  constructor() {
    super();
    this.subscribe(() => this._registerToActions.bind(this))
    this._user = null;
    this._jwt = null;
  }

  subscribe(actionSubscribe) {
    this._dispatchToken = AppDispatcher.register(actionSubscribe());
  }

  get dispatchToken() {
    return this._dispatchToken;
  }

  emitChange() {
    this.emit('CHANGE');
  }

  addChangeListener(cb) {
    this.on('CHANGE', cb)
  }

  removeChangeListener(cb) {
    this.removeListener('CHANGE', cb);
  }

  _registerToActions(action) {
    switch(action.actionType) {
      case 'LOGIN_USER':
        this._jwt = action.jwt;
        this._user = jwt_decode(this._jwt);
        this.emitChange();
        localStorage.setItem('jwt', action.jwt);
        localStorage.setItem('user', JSON.stringify(this._user));
        break;
      case 'LOGOUT_USER':
        this._user = null;
        this.emitChange();
        localStorage.removeItem('jwt');
        localStorage.removeItem('user');
        break;
      default:
        break;
    };
  }

  get user() {
    return this._user;
  }

  get jwt() {
    return this._jwt;
  }

  isLoggedIn() {
    if (!this._user) {
      var jwt = localStorage.getItem('jwt')
      if (!jwt) {
        return false;
      } else {
        this._jwt = jwt;
      }
      var user = JSON.parse(localStorage.getItem('user'));
      if (!user) {
        return false;
      } else {
        console.log(user);
        this._user = user;
      }

    }

    return !!this._user;
  }
}

export default new LoginStore();
