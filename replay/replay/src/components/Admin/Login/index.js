import React, { PropTypes, Component } from 'react';
import Auth from '../Service/AuthService'
import { Button, Input } from 'antd';

import './style.css';

export default class Login extends Component {

  constructor(props) {
    super(props);

    this.state = {
      username: "",
      password: "",
      error: ""
    };

    this.handleUsernameChange = this.handleUsernameChange.bind(this);
    this.handlePasswordChange = this.handlePasswordChange.bind(this);
    this.loginCallback = this.loginCallback.bind(this);
    this.login = this.login.bind(this);
  }

  passwordValueLink = (value) => {
    return {
        value: this.state.password,
        requestChange: this.handlePasswordChange
    };
  }

  handlePasswordChange(newpassword) {
      this.setState({password: newpassword});
  }

  usernameValueLink = (value) => {
      return {
          value: this.state.username,
          requestChange: this.handleUsernameChange
      };
  }

  handleUsernameChange(newUsername) {
    this.setState({username: newUsername});
  }

  loginCallback(error) {
    this.setState({error: error.error});
  }

  login(e) {
    this.setState({error: ""});
    e.preventDefault();
    Auth.login(this.state.username, this.state.password, this.loginCallback);
  }

  render() {
    return (
      <div className="login-view-content-area center-div">
        <h1 className="page-title">Admin panel log in</h1>
        <form role="form">
          <div className="form-group">
           <Input className="login-input" type="text" valueLink={this.usernameValueLink()}placeholder="Username" />
           <Input className="login-input" type="password" valueLink={this.passwordValueLink()} placeholder="Password" />
         </div>
         <p className="error-red">{this.state.error}</p>
         <Button type="submit" onClick={this.login.bind(this)}>Submit</Button>
        </form>
      </div>
    );
  }
}

Login.contextTypes = {
  api_url: PropTypes.string
};
