import React, { Component } from 'react';
import AuthenticatedComponent from '../AuthenticatedComponent'
import TeamInsertForm from './InsertForm'

import './style.css';

export default AuthenticatedComponent(class AdminTeam extends Component {

  constructor(props) {
    super(props);
    this.state = {
      practices: []
    };
  }

  render() {

    return (
      <div className="admin-content center-div">
        <h1 className="page-title">Manage teams</h1>
        <div className="admin-content-div">
          <div className="admin-content-subdiv">
            <TeamInsertForm jwt={this.props.jwt}/>
          </div>
        </div>
      </div>
    );
  }
});
