import React, {PropTypes, Component} from 'react'
import {Input, InputNumber, Button, Select} from 'antd'
import axios from 'axios';

const Option = Select.Option;

export default class TeamInsertForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      teams: [],
      team_name: "",
      tag_id: 0,
      erreur: "",
      selectedTeamName: "",
      selectedTeamId: "",
      selectedTeamTagId: 0
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleTeamNameChange = this.handleTeamNameChange.bind(this);
    this.handleTagIdChange = this.handleTagIdChange.bind(this);
    this.selectOnChange = this.selectOnChange.bind(this);
    this.submitTeamChange = this.submitTeamChange.bind(this);
    this.handleTagNewIdChange = this.handleTagNewIdChange.bind(this);
    this.handleTeamNewNameChange = this.handleTeamNewNameChange.bind(this);
  }

  componentDidMount() {
    axios.get(this.context.api_url + "team"
    ).then(res => {
          this.setState({teams: res.data})
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({erreur: ""})
    axios.put(this.context.api_url + "admin/team", {
      token: this.props.jwt,
      team_name: this.state.team_name,
      tag_id: this.state.tag_id
    }).then(res => {
      if (!res.data.success) {
          this.setState({erreur: res.data.message})
      }
      console.log(res.data)
    });
  }

  selectOnChange(val) {
    this.state.teams.map((team) => {
        if (team.team_id == val) {
          this.setState({
            selectedTeamId : team.team_id,
            selectedTeamName : team.team_name,
            selectedTeamTagId : team.tag_id
          });
          return;
        }
    });
  }

  submitTeamChange() {
    console.log("submit!" + this.state.selectedTeamId + " " + this.state.selectedTeamName + " " + this.state.selectedTeamTagId);

    axios.post(this.context.api_url + "admin/team/" + this.state.selectedTeamId,
     {
       token: this.props.jwt,
       team_name: this.state.selectedTeamName,
       tag_id: this.state.selectedTeamTagId
     }
    ).then(res => {
        if (!res.data.success) {
            this.setState({erreur: res.data.message})
            return;
        }
        axios.get(this.context.api_url + "team").then(res => {
              this.setState({teams: res.data})
        });
    });
  }

  static contextTypes = {
    api_url: PropTypes.string
  };

  search(inputValue, option) {
    return option.props.start_string.toLowerCase().includes(inputValue.toLowerCase());
  }

  /*Change handlers*/

  handleTeamNameChange(event) {
    this.setState({team_name: event.target.value});
  }

  handleTagIdChange(value) {
    this.setState({tag_id: value});
  }

  handleTeamNewNameChange(event) {
    this.setState({selectedTeamName: event.target.value});
  }

  handleTagNewIdChange(value) {
    this.setState({selectedTeamTagId: value});
  }

  simpleFilter(input, option) {
     return (option.value).toLowerCase().indexOf(input.toLowerCase()) >= 0
  };

  render() {
      return (
        <div>
        <p className="error-red">{this.state.erreur}</p>
        <h2 className="sub-para">Add new team</h2>
        <form className="team-add-form" onSubmit={this.handleSubmit}>
            <p>Team name:</p><Input value={this.state.team_name} onChange={this.handleTeamNameChange} type="text" placeholder="Team name" name="team_name"/>
            <p>Tag id:</p><InputNumber value={this.state.tag_id} onChange={this.handleTagIdChange} min={0} defaultValue={0} name="tag_id" />
            <Button htmlType="submit">Submit</Button>
        </form>
        <h2 className="sub-para">Change team</h2>
          <Select  className="team-selector" notFoundContent="None found" showSearch placeholder="Select team" onChange={this.selectOnChange} filterOption={this.simpleFilter}>
              {this.state.teams.map(team =>
                  <Option value={"" + team.team_id} key={team.team_id}>{team.team_name}</Option>
              )}
          </Select>
        <div className="team-add-form">
          <p>Team name:</p><Input value={this.state.selectedTeamName} onChange={this.handleTeamNewNameChange} type="text" placeholder="Team name" name="new_team_name"/>
          <p>Tag id:</p><InputNumber value={this.state.selectedTeamTagId} onChange={this.handleTagNewIdChange} min={0} defaultValue={0} name="new_tag_id" />
          <Button onClick={this.submitTeamChange}>Submit</Button>
        </div>
        </div>
      );
  };
}
