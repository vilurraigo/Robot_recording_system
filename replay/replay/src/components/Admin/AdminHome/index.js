import React, { PropTypes, Component } from 'react';
import axios from 'axios';
import AuthenticatedComponent from '../AuthenticatedComponent'

import './style.css';

export default AuthenticatedComponent(class AdminHome extends Component {

  static contextTypes = {
    api_url: PropTypes.string
  };

  componentWillMount() {
    axios.get(this.context.api_url + "admin/home", {params: {token: this.props.jwt}})
      .then(res => {
        console.log(res);
      });
  }

  render() {
    return (
      <div className="admin-content center-div">
        <h1 className="page-title">Admin control panel</h1>
        <p>Welcome home, {this.props.user.username}</p>
        <div className="admin-content-div"></div>
      </div>
    );
  }
});
