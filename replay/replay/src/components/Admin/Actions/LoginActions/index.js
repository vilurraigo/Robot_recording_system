import AppDispatcher from '../../AppDispatcher';
import {browserHistory} from 'react-router'

export default {
  loginUser: (jwt) => {
    var savedJwt = localStorage.getItem('jwt');

    AppDispatcher.dispatch({
      actionType: 'LOGIN_USER',
      jwt: jwt
    });

    if (savedJwt !== jwt) {


      browserHistory.push("/admin/home");
      localStorage.setItem('jwt', jwt);
    }
  },
  logoutUser: () => {
    browserHistory.push('/admin/login');
    localStorage.removeItem('jwt');
    AppDispatcher.dispatch({
      actionType: 'LOGOUT_USER'
    });
  }
}
