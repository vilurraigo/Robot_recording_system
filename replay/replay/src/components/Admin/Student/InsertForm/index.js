import React, {PropTypes, Component} from 'react'
import {Input, Button, Select} from 'antd'
import axios from 'axios';

const Option = Select.Option;

export default class StudentInsertForm extends Component {

  constructor(props) {
    super(props);
    this.state = {
      student_id: "",
      student_name: "",
      erreur: ""
    }

    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleStudentNameChange = this.handleStudentNameChange.bind(this);
    this.handleStudentIdChange = this.handleStudentIdChange.bind(this);
  }

  componentDidMount() {
  }

  handleSubmit(e) {
    e.preventDefault();
    this.setState({erreur: ""})
    axios.post(this.context.api_url + "admin/student", {
      token: this.props.jwt,
      student_name: this.state.student_name,
      student_id: this.state.student_id
    }).then(res => {
      if (!res.data.success) {
          this.setState({erreur: res.data.message})
      }
      console.log(res.data)
    });
  }

  static contextTypes = {
    api_url: PropTypes.string
  };

  search(inputValue, option) {
    return option.props.name.toLowerCase().includes(inputValue.toLowerCase());
  }

  /*Change handlers*/

  handleStudentNameChange(event) {
    this.setState({student_name: event.target.value});
  }

  handleStudentIdChange(event) {
    this.setState({student_id: event.target.value});
  }

  render() {
      return (
        <form className="team-add-form" onSubmit={this.handleSubmit}>
            <h2>Add student</h2>
            <p className="error-red">{this.state.erreur}</p>
            <p>Student name:</p><Input value={this.state.student_name} onChange={this.handleStudentNameChange} type="text" placeholder="Student name"/>
            <p>Student id:</p><Input value={this.state.student_id} onChange={this.handleStudentIdChange} />
            <Button id="student-form-submit-button" htmlType="submit">Submit</Button>
        </form>
      );
  };
}
