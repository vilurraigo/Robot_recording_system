import React, { PropTypes, Component } from 'react';
import axios from 'axios';
import AuthenticatedComponent from '../AuthenticatedComponent'
import Auth from '../Service/AuthService'
import {Button} from 'antd'
import StudentInsertForm from './InsertForm'
import StudentSearch from './Search'

import './style.css';

export default AuthenticatedComponent(class AdminStudent extends Component {

  constructor(props) {
    super(props);
  }

  static contextTypes = {
    api_url: PropTypes.string
  };

  componentWillMount() {
    axios.get(this.context.api_url + "admin/home", {params: {token: this.props.jwt}})
      .then(res => {
        console.log(res);
      });
  }

  render() {
    return (
      <div className="admin-content center-div">
        <h1 className="page-title">Manage Students</h1>
        <div className="admin-content-div">
          <div className="admin-content-subdiv">
            <StudentInsertForm jwt={this.props.jwt} />
          </div>
          <div className="admin-content-subdiv extra-top-margin">
            <StudentSearch jwt={this.props.jwt} />
          </div>
        </div>
      </div>
    );
  }
});
