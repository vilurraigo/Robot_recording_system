import React, { PropTypes, Component } from 'react';
import axios from 'axios';
import {Select, Button} from 'antd'

const Option = Select.Option;

export default class StudentSearch extends Component {

  constructor(props) {
    super(props);
    this.state = {
      students: [],
      teams: [],
      erreur: ""
    };
    this.selectOnChange = this.selectOnChange.bind(this);
    this.teamSelectOnChange = this.teamSelectOnChange.bind(this);
    this.deleteStudent = this.deleteStudent.bind(this);
    this.removeFromTeam = this.removeFromTeam.bind(this);
    this.onAddToTeam = this.onAddToTeam.bind(this);
  }

  static contextTypes = {
    api_url: PropTypes.string
  };

  componentWillMount() {
    axios.get(this.context.api_url + "admin/student", {params: {token: this.props.jwt}}
    ).then(res => {
          this.setState({students: res.data})
    });
    axios.get(this.context.api_url + "team"
    ).then(res => {
          this.setState({teams: res.data})
    });
  }

  selectOnChange(val) {
    axios.get(this.context.api_url + "admin/student/" + val + '/teams', {params: {token: this.props.jwt}}
    ).then(res => {
      this.state.students.map((student) => {
          if (student.student_id === val) {
            student.teams = res.data;
            this.setState({selectedStudent : student});
            return;
          }
      });
    });
    return;
  }

  teamSelectOnChange(val) {
    this.setState({selectedTeamId: val})
  }

  onAddToTeam() {
    this.setState({erreur: ""})
    if (!this.state.selectedTeamId) {
      return;
    }
    axios.post(this.context.api_url + "admin/team/" + this.state.selectedTeamId + "/student/" + this.state.selectedStudent.student_id,
     {
       token: this.props.jwt
     }
    ).then(res => {
        if (!res.data.success) {
            this.setState({erreur: res.data.message})
            return;
        }
        this.selectOnChange(this.state.selectedStudent.student_id);
        }
    );
  }

  removeFromTeam(studentID, teamID) {
    this.setState({erreur: ""})
    axios.delete(this.context.api_url + "admin/team/" + teamID + "/student/" + studentID, {
      params: {token: this.props.jwt}
    }).then(res => {
          if (!res.data.success) {
              this.setState({erreur: res.data.message})
              return;
          }
          var newSelectedStudent = this.state.selectedStudent;
          for (var i = 0; i < newSelectedStudent.teams.length; i++) {
            if (newSelectedStudent.teams[i].team_id === teamID)
                 newSelectedStudent.teams.splice(i, 1);
          }
          this.setState({selectedStudent: newSelectedStudent})
        }
    );
  }

  deleteStudent() {
    this.setState({erreur: ""})
    var deleteStudent = this.state.selectedStudent;
    axios.delete(this.context.api_url + "admin/student/" + deleteStudent.student_id, {params: {token: this.props.jwt}}
    ).then(res => {
          if (!res.data.success) {
              this.setState({erreur: res.data.message})
              return;
          }
          var newStudents = this.state.students;
          for (var i = 0; i < newStudents.length; i++) {
            if (newStudents[i].student_id === deleteStudent.student_id
               && newStudents[i].team_id === deleteStudent.team_id)
                 newStudents.splice(i, 1);
          }
          this.setState({students: newStudents, selectedStudent: undefined})
        }

    );
  }

  filter(input, option) {
     return (option.props.student.student_name + " " + option.props.student.student_id).toLowerCase().indexOf(input.toLowerCase()) >= 0
  };

  simpleFilter(input, option) {
     return (option.value).toLowerCase().indexOf(input.toLowerCase()) >= 0
  };

  render() {
    //TODO beautify
    if (this.state.selectedStudent) {
      console.log(this.state.selectedStudent)
      return (
        <div>
        <h2>View students</h2>
        <p className="error-red">{this.state.erreur}</p>
        <Select className="student-selector" notFoundContent="None found" showSearch placeholder="View student" onChange={this.selectOnChange} filterOption={this.filter} >
            {this.state.students.map(student =>
                <Option value={"" + student.student_id} key={student.student_id} student={student}>{student.student_name + ' ' + student.student_id} </Option>
            )}
        </Select>
        <table className="student-info-table">
          <tbody>
              <tr><td>Name</td><td id="sudent_name">{this.state.selectedStudent.student_name}</td></tr>
              <tr><td>Student ID</td><td id="student_id">{this.state.selectedStudent.student_id}</td></tr>
          </tbody>
        </table>
        <h3 className="student-team-table-header">Student's teams</h3>
        <table className="student-team-table">
            <tbody>
            {this.state.selectedStudent.teams.map(team =>
              <tr key={team.team_id}><td>{team.team_name}</td><td><Button onClick={() => this.removeFromTeam(this.state.selectedStudent.student_id, team.team_id)} type="danger">Remove</Button></td></tr>
            )}
          </tbody>
        </table>
        <h3 className="student-team-table-header">Add student to team</h3>
          <table className="student-team-table">
              <tbody>
                <tr><td>
                  <Select  className="student-add-team-selector" notFoundContent="None found" showSearch placeholder="Select team" onChange={this.teamSelectOnChange} filterOption={this.simpleFilter}>
                      {this.state.teams.map(team =>
                          <Option value={"" + team.team_id} key={team.team_id}>{team.team_name}</Option>
                      )}
                  </Select>
                </td><td><Button onClick={this.onAddToTeam}>Add team</Button></td></tr>
            </tbody>
          </table>
        <Button onClick={this.deleteStudent} type="danger">Delete student</Button>
        </div>
      );
    } else {
      return (
        <div>
        <h2>View students</h2>
        <Select className="student-selector" notFoundContent="None found" showSearch placeholder="View student" onChange={this.selectOnChange} filterOption={this.filter} >
            {this.state.students.map(student =>
                <Option value={"" + student.student_id} key={student.student_id} student={student}>{student.student_name + ' ' + student.student_id} </Option>
            )}
        </Select>
      </div>
      )
    }

  }
}
