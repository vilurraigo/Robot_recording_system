import LoginActions from '../../Actions/LoginActions';
import axios from 'axios';

class AuthService {

  login(username, password, errorCallback) {
    axios.post("http://localhost:9000/api/v1/authenticate", {
      username: username,
      password: password
    }).then(res => {
        var jwt = res.data.token;
        if (jwt) {
          LoginActions.loginUser(jwt);
        } else {
          errorCallback({success: false, error:  res.data.message});
        }

    }).catch(() => {
        errorCallback({success: false, error:  "network error"});
    });
  }

  logout() {
    LoginActions.logoutUser();
  }


  handleAuth(loginPromise) {
    return loginPromise
      .then(function(response) {
        var jwt = response.data.token;
        LoginActions.loginUser(jwt);
        return true;
      });
  }
}

export default new AuthService()
