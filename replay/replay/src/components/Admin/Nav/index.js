import React, {Component} from 'react';
import { Link } from 'react-router';
import {Button} from 'antd'
import Auth from '../Service/AuthService'

import './style.css';

export default class AdminNav extends Component {


  logout() {
      Auth.logout();
  }


  render() {
    return (
    <nav className="adm-navbar" >
        <div className="adm-navbar-left">
          <ul className="adm-navbar-nav">
            <li><Link id="admin-nav-team" to="/admin/team">Manage Teams</Link></li>
            <li><Link id="admin-nav-student" to="/admin/student">Manage Students</Link></li>
            <li><Button id="logout-btn" onClick={this.logout}> Log out </Button></li>
          </ul>
        </div>
    </nav>
    );
  }

}
