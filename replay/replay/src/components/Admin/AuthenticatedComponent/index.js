import React, {Component} from 'react';
import {browserHistory} from 'react-router'
import LoginStore from '../Store/LoginStore';
import AdminNav from '../Nav'

export default (ComposedComponent) => {
  return class AuthenticatedComponent extends Component {

    componentWillMount() {

      if (!LoginStore.isLoggedIn()) {
        browserHistory.push("/admin/login");
      }
      this.state = this._getLoginState();
    }

    componentWillUpdate() {
      if (!LoginStore.isLoggedIn()) {
        browserHistory.push("/admin/login");
      }
    }

    _getLoginState() {
      return {
        userLoggedIn: LoginStore.isLoggedIn(),
        user: LoginStore.user,
        jwt: LoginStore.jwt
      };
    }

    componentDidMount() {
      LoginStore.addChangeListener(this._onChange.bind(this));
    }

    _onChange() {
      this.setState(this._getLoginState());
    }

    componentWillUnmount() {
        LoginStore.removeChangeListener(this._onChange.bind(this));
    }

    render() {
      if (!this.state.userLoggedIn) {
        return (<div></div>);
      }
      return (
      <div>
        <AdminNav />
        <ComposedComponent
          {...this.props}
          user={this.state.user}
          jwt={this.state.jwt}
          userLoggedIn={this.state.userLoggedIn} />
      </div>
      );
    }
  }
};
