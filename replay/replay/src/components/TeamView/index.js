import React, { PropTypes, Component } from 'react';
import axios from 'axios';
import {Input} from 'antd';

import TeamLink from './TeamLink'
import './style.css';

class App extends Component {

  constructor(props) {
    super(props);

    this.state = {
      filteredTeams: [],
      teams: []
    };

    this.filter = this.filter.bind(this);
  }

  filter(event) {
    var filteredTeams = [];

    this.state.teams.map((team) => {
      if (team.team_name.includes(event.target.value)) {
        filteredTeams.push(team);
      }
      return true;
    });
    this.setState({filteredTeams : filteredTeams})
  }

  componentWillMount() {
    axios.get(this.context.api_url + "team")
      .then(res => {
        this.setState({
          teams: res.data,
          filteredTeams: res.data
        });
      });
  }

  render() {
    return (
      <div>
        <h1 className="page-title">Teams</h1>
        <Input className="team-filter" placeholder="Filter" onChange={this.filter} />
        {this.state.filteredTeams.map((team) =>
            <TeamLink teamData={team} key={team.team_id} />
        )}
      </div>
    );
  }
}

export default App;

App.contextTypes = {
  api_url: PropTypes.string
};
