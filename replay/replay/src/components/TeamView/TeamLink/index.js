import React, { Component } from 'react';
import { Card } from 'antd';
import { browserHistory } from 'react-router';

import './style.css';

export default class RunLink extends Component {

  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(team) {
      browserHistory.push("/teams/" + this.props.teamData.team_id);
  }

  render() {
    const teamData = this.props.teamData;
    return (
      <Card onClick={this.handleClick} className="main-card" title={teamData.team_name}>Tag ID: {teamData.tag_id}</Card>
    );
  }
}
