import React, { Component, PropTypes } from 'react';
import Nav from '../Nav';

//TODO remove this if it is clear why it wasn't included the normal way.
import './antd.css';

export default class Container extends Component  {

  getChildContext() {
    return {api_url: "http://localhost:9000/api/v1/"};
  }


  render() {
    return (

      <div>
        <Nav/>
        {this.props.children}
      </div>
    )
  }
};


Container.childContextTypes = {
  api_url: PropTypes.string
};
