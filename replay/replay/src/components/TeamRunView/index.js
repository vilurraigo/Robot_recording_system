import React, { PropTypes, Component } from 'react';
import axios from 'axios';

import RunLink from '../App/RunLink'
//import './style.css';

export default class TeamRunView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      runs: []
    };
  }

  componentWillMount() {
    axios.get(this.context.api_url + "team/" + this.props.params.team_id  + "/run/")
      .then(res => {
        this.setState({
          runs: res.data
        });
      });
  }

  render() {
    return (
      <div>
        <h1 className="page-title">All runs</h1>
        {this.state.runs.map((run) =>
            <RunLink runData={run} key={run.run_id} />
        )}
      </div>
    );
  }
}


TeamRunView.contextTypes = {
  api_url: PropTypes.string
};
