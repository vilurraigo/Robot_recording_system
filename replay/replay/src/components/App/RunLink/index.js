import React, { Component } from 'react';
import { Card } from 'antd';
import dateFormat from 'dateformat';
import { browserHistory } from 'react-router';

import './style.css';

export default class RunLink extends Component {

  constructor() {
    super();
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick(team) {
      browserHistory.push("/view-run/" + this.props.runData.run_id);
  }


  render() {
    const runData = this.props.runData;
    if (!runData.team_name) {
      runData.team_name = "Team not set!"
    }
    return (
      <Card className="main-card" onClick={this.handleClick} title={dateFormat(runData.run_time, "dd.mm.yyyy HH:MM") + " - " + runData.team_name}>Click to view</Card>
    );
  }
}
