import React, { PropTypes, Component } from 'react';
import axios from 'axios';
import {DatePicker} from 'antd';
import moment from 'moment';
import RunLink from './RunLink'
import './style.css';

const { MonthPicker, RangePicker } = DatePicker;

class App extends Component {

  constructor(props) {
    super(props);
    var dateTwoDaysAgo = new Date();
    dateTwoDaysAgo.setDate(dateTwoDaysAgo.getDate() - 2);
    this.state = {
      runs: [],
      lowerLimit: dateTwoDaysAgo,
      upperLimit: new Date()
    };

    this.getRuns = this.getRuns.bind(this);

    this.onDateChange = this.onDateChange.bind(this);
  }

  componentWillMount() {
  }

  getRuns() {
    axios.get(this.context.api_url + "run")
      .then(res => {
        this.setState({
          runs: res.data
        });
      });
  }

  onDateChange([lowerLimit, upperLimit], dateString) {
      console.log(dateString);
      lowerLimit.set({hour: 0, minute: 0, second: 0, millisecond: 0});
      upperLimit.set({hour: 23, minute: 59, second: 59, millisecond: 999});

      axios.get(this.context.api_url + "run", {params: {lowerLimit: lowerLimit, upperLimit: upperLimit}})
        .then(res => {
          this.setState({
            runs: res.data
          });
        });
  }

  render() {
    return (
      <div className="common-center-div">
        <h1 className="page-title">All runs</h1>
        <RangePicker defaultValue={[moment(this.state.lowerLimit, 'YYYY-MM-DD '), moment(this.state.upperLimit, 'YYYY-MM-DD')]} onChange={this.onDateChange} placeholder={["Start date", "End date"]} />
        {this.state.runs.map((run) =>
            <RunLink runData={run} key={run.run_id} />
        )}
      </div>
    );
  }
}

export default App;

App.contextTypes = {
  api_url: PropTypes.string
};
