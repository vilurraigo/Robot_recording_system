import React, { Component } from 'react';
import classnames from 'classnames';

import './style.css';

export default class About extends Component {
  // static propTypes = {}
  // static defaultProps = {}
  // state = {}

  render() {
    const { className, ...props } = this.props;
    return (
      <div className="common-center-div">
        <h1 className="page-title">About</h1>
        <p className="extra-top-margin">Put something informative here ...</p>
      </div>
    );
  }
}
