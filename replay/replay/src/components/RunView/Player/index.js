import React, {PropTypes, Component} from 'react'
import { Slider, Button } from 'antd';
import axios from 'axios';

export default class Player extends Component {

  constructor(props) {
    super(props);
    this.state = {
      slider_value: 0,
    }
    this.images = [];
    this.last_frame_id = -1;
    this.renderedImageId = 0;
    this.playing = false;
    this.hasDownloadStarted = false;

    this.updateCanvas = this.updateCanvas.bind(this);
    this.hasAllRunData = this.hasAllRunData.bind(this);
    this.nextImage = this.nextImage.bind(this);
    this.restart = this.restart.bind(this);
    this.onSliderChange = this.onSliderChange.bind(this);

    this.play = this.play.bind(this);
    this.autoplay = this.autoplay.bind(this);
    this.stop = this.stop.bind(this);
    this.pause = this.pause.bind(this);

    this.imgType = "data:image/png;base64,";
  }

  downloadImage(run_id, curr_id, max_id, self) {
    if (!max_id || curr_id > max_id) {
      return;
    }
    var firstRendered = false;
    for (var i = 0; i <= max_id; i++) {
      var img = new Image();
      img.src = self.context.api_url + "images/"+ run_id +"/" + i +".png"
      this.images.push(img);

      if (!self.hasDownloadStarted) {
        self.updateCanvas();
        self.hasDownloadStarted = true;
      }

      //TODO set a % of video
      /*if (i > 10 && !firstRendered) {
        self.updateCanvas();
        firstRendered = true;
      }*/
    }

  }

  static contextTypes = {
    api_url: PropTypes.string
  };

  nextImage() {
    if (this.renderedImageId !== this.last_frame_id) {
      this.renderedImageId++;
      //this.setState({slider_value: this.renderedImageId});
      this.setState({slider_value: this.renderedImageId});
      this.updateCanvas()
    } else {
      //TODO stop
    }
  }

  restart() {
    this.renderedImageId = 0;
    this.updateCanvas();
  }

  updateCanvas() {
      const ctx = this.canvas.getContext('2d');
      var cw = ctx.canvas.width;
      var ch = ctx.canvas.height;
      if (this.renderedImageId >= 0 && this.renderedImageId <= this.last_frame_id) {
        ctx.drawImage(this.images[this.renderedImageId], 0, 0, cw, ch);
      }
  }

  autoplay(delay) {
      var interval = setInterval(function(self) {
        if (!self.playing) {
          clearInterval(interval);
          return;
        }
        self.nextImage();
      }, delay, this);
  }

  /*User controls:*/

  play() {
    if (this.playing) {
      return;
    }
    var delay = 1000 / this.props.run.framerate;
    this.playing = true;
    this.autoplay(delay);
  }

  stop() {
    this.playing = false;
    this.restart();
  }

  pause() {
    this.playing = false;
  }

  hasAllRunData() {
    this.last_frame_id = this.props.run.last_frame_id;

    if (!this.last_frame_id) {
      return;
    }
    if (!this.hasDownloadStarted) {
      this.downloadImage(this.props.run.run_id, 0, this.last_frame_id, this);
    }
  }

  onSliderChange(value) {
    this.pause();
    this.setState({slider_value: value});


    if (value == this.renderedImageId) {
      return;
    }

    this.renderedImageId = value;
    this.updateCanvas();
    //this.setState({slider_value: value});
  }

  render() {

    if (this.props.run) {
      this.hasAllRunData();
    }

      return (
        <div>
        <div className="run-view-wrapper">
          <canvas ref={canvas => this.canvas = canvas} id="run-view-canvas"></canvas>
        </div>
        <Button icon="play-circle" type="primary" id="play-button" onClick={this.play} className="run-view-action-buttons"></Button>
        <Button type="primary" icon="pause-circle" id="pause-button" onClick={this.pause} className="run-view-action-buttons"></Button>
        <Slider min={0} max={this.last_frame_id} onChange={this.onSliderChange} value={this.state.slider_value} />
        </div>
      );
  };
}
