import React, { PropTypes, Component } from 'react';
import axios from 'axios';
import {Input, InputNumber, Button, Select} from 'antd'

import AdminFeatures from './AdminFeatures'
import Player from './Player'

import './style.css';

export default class RunView extends Component {

  constructor(props) {
    super(props);

    this.state = {
      run: {}
    };

    this.setTeamName = this.setTeamName.bind(this);
  }

  setTeamName(team_name) {
    var newRun = this.state.run;
    newRun.team_name = team_name;
    this.setState({run : newRun})
  }

  componentWillMount() {
    var run_id = this.props.params.run_id;
    axios.get(this.context.api_url + "run/" + run_id)
      .then(res => {
        this.setState({
          run: res.data
        });
      });
  }


  render() {
    var run = this.state.run;
    return (
      <div className="run-view-content-area">
          <h1 className="page-title run-view-title">View Run</h1>
          <div>
            <div className="run-view-left-div">
              <Player run={run} />
            </div>
            <div className="run-view-right-div">
                <h2>Run info</h2>
                <table className="run-view-info-table">
                  <tbody>
                    <tr>
                      <td>Task Name:</td><td>{run.task_name}</td>
                    </tr>
                    <tr>
                      <td>Team name:</td><td>{run.team_name}</td>
                    </tr>
                    <tr>
                      <td>Score:</td><td>{run.score}</td>
                    </tr>
                    <tr>
                      <td>Time (s):</td><td>{run.ms_taken}</td>
                    </tr>
                    <tr>
                      <td>Distance traveled:</td><td>{run.distance}</td>
                    </tr>
                    <tr>
                      <td>Run data:</td><td>{JSON.stringify(run.run_data)}</td>
                    </tr>
                  </tbody>
                </table>
                <AdminFeatures run={run} setTeamName={this.setTeamName}/>
            </div>
          </div>
      </div>
    );
  }
}


RunView.contextTypes = {
  api_url: PropTypes.string
};
