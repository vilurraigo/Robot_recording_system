import React, { PropTypes, Component } from 'react';
import axios from 'axios';
import LoginStore from '../../Admin/Store/LoginStore'
import {Button, Select} from 'antd'

const Option = Select.Option;

export default class AdminFeatures extends Component {

  constructor(props) {
    super(props);
    var loggedIn = LoginStore.isLoggedIn();
    this.state = {
      teams: [],
      jwt: LoginStore.jwt,
      loggedIn: loggedIn,
      selectValue: "",
      selectedTeam: {},
      erreur: ""
    };
    this.selectOnChange = this.selectOnChange.bind(this);
    this.changeTeam = this.changeTeam.bind(this);
  }

  static contextTypes = {
    api_url: PropTypes.string
  };

  componentWillMount() {
    if (this.state.loggedIn) {
      axios.get(this.context.api_url + "team")
        .then(res => {
          this.setState({teams: res.data})
        });
    }
  }

  selectOnChange(val) {
    this.setState({selectValue: val});
  }

  changeTeam() {
    this.setState({erreur: ""})
    axios.post(this.context.api_url + "admin/run/" + this.props.run.run_id + "/team/" + this.state.selectValue, {
      token: this.state.jwt
    }).then((res) => {
      console.log(res.data);
      if (res.data.success) {
        this.props.setTeamName(res.data.team_name)
      } else {
        this.setState({erreur: res.data.message})
      }
    });
  }

  filter(input, option) {
     return option.props.name.toLowerCase().indexOf(input.toLowerCase()) >= 0
  };

  render() {
    if (this.state.loggedIn) {
      return (
        <div>
        <h3>Admin actions</h3>
        <Select style={{width: "100%"}} notFoundContent="None found" showSearch placeholder="Change team" onChange={this.selectOnChange} filterOption={this.filter} >
            {this.state.teams.map(team =>
                <Option value={"" + team.team_id} key={team.team_id} team={team.team_name}>{team.team_name} </Option>
            )}
        </Select>
        <Button onClick={this.changeTeam}>Change team</Button>
        <p className="error-red">{this.state.erreur}</p>
        </div>
      );
    } else {
      return (
        <div></div>
      );
    }
  }
}
