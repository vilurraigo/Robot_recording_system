import React, { Component } from 'react';
import classnames from 'classnames';

import './style.css';

export default class NotFound extends Component {
  // static propTypes = {}
  // static defaultProps = {}
  // state = {}

  render() {
    const { className, ...props } = this.props;
    return (
      <div className="common-center-div not-found-height">
        <h1>
          <strong className="not-found-404-large">404</strong> <small>Not Found <sub>:(</sub></small>
        </h1>
      </div>
    );
  }
}
