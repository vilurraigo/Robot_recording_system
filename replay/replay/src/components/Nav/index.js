import React, {Component} from 'react';
import { Link } from 'react-router';

import './style.css';

export default class Nav extends Component {

  render() {
    return (
    <nav className="navbar" >
        <div className="navbar-left">
          <ul className="navbar-nav">
            <li><Link id="all-runs-nav-link" to="/">All runs</Link></li>
            <li><Link id="teams-nav-link" to="/teams">Teams</Link></li>
            <li><Link id="admin-nav-link" to="/admin/home">Admin</Link></li>
            <li><Link id="about-nav-link" to="/about">About</Link></li>
          </ul>
        </div>
    </nav>
    );
  }

}
