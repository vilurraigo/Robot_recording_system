import React from 'react';
import { Router, Route} from 'react-router';

import App from './components/App';
import NotFound from './components/NotFound';
import About from './components/About';
import Container from './components/Container';
import RunView from './components/RunView';
import TeamView from './components/TeamView';
import TeamRunView from './components/TeamRunView';
import Login from './components/Admin/Login';
import AdminHome from './components/Admin/AdminHome';
import AdminTeam from './components/Admin/Team';
import AdminStudent from './components/Admin/Student';


const Routes = (props) => (
  <Router {...props}>
    <Route component={Container} >
      <Route path="/" component={App} />
      <Route path="/view-run/:run_id" component={RunView} />
      <Route path="/teams" component={TeamView} />
      <Route path="/teams/:team_id" component={TeamRunView} />
      <Route path="/admin/login" component={Login} />
      <Route path="/admin/home" component={AdminHome} />
      <Route path="/admin/team" component={AdminTeam} />
      <Route path="/admin/student" component={AdminStudent} />
      <Route path="/about" component={About} />
      <Route path="/*" component={NotFound} />
    </Route>
  </Router>
);

export default Routes;
